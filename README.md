# Engine Project

Thanks for checking out my engine project!  
Here are some general things about the engine:
* C++ 17 
* SDL2 for windowing, input, image loading
* Vulkan renderer
  * Supports the `VK_GOOGLE_display_timing` extension if available (no graphics drivers currently support it :c, but mesa patches exist)
  * Take a look at [this article](https://medium.com/@alen.ladavac/the-elusive-frame-timing-168f899aec92) by the Serious Sam, Talos Principle people on why this extension should be supported.
* cgltf.h for glTF 2.0 model loading (<https://github.com/jkuhlmann/cgltf>)
* GLM for 3D Math (<https://glm.g-truc.net/0.9.9/index.html>)
* Custom ECS implementation inspired by Blizzard's ECS talk and Amethyst
* A command system inspired by Quake

## Running

Download an app release from the releases page for your platform, extract it to a directory, and run the executable.  
Working configurations are: Linux 64-bit, Windows 64-bit (doesn't depend on any Windows 10 nonsense), and Wine.  

### Runtime Dependencies

On Linux, you'll need SDL2 and SDL2_Image installed on your system.  
On Windows, there shouldn't be any external system dependencies required, as most libraries are statically linked. However, your system and graphics card obviously needs to support vulkan.  
**If your system does support vulkan, but an app crashes or doesn't launch, let me know in the issues or on discord, I'd really appreciate it!**

## Structure and Layout

### `./`  
Build scripts, git repo, readmes, license


### `engine/`  
Engine library. Namespace `engine`. Builds `libengine.a` (creative names, I know).


### `apps/`
Examples and applications that use the `engine` as a static library.  
No code in `engine/` can depend on code in `apps/`.

## Building

If you just want to run the apps, I'd recommend downloading a build, but here are the instructions anyway:

Right now, it is possible to build Linux and Windows binaries (via mingw-w64) from Linux. However, you will probably have to edit the `CMakeLists.txt` files, as they are heavily based on my own configuration.  
I have never tried to build with MSVC, but it should be possible to set up a visual studio solution and build the project, but if possible, I would recommend building in wsl.

To build the project, make a build directory (e.g. `mkdir build` in the project root), and run cmake on the root directory from that directory (e.g. `cd build`, `cmake ../`).  
Once the build files are created, build with `make`, and build the shaders with `make shaders`. To run `modeltest` (`modeltest.exe`), you must copy or link the `assets` directory from the releases page into the working directory.  
Now you can run the apps from their directories in `apps/`. Be sure to run them from the root of the `build` directory, as the compiled shaders must be in the working directory. Alternatively, copy the `.spv` files into each app's directory.

### Build Dependencies

Linux: 
* A compiler that supports `-std=g++17`. I use gcc, cmake should work.
* SDL2 (must include development libraries, look for a `-dev` package for your distro)
* SDL2\_Image (again, must have development libraries)
* GLM
* Vulkan SDK (make sure `glslangValidator` is in your path)

MinGW-w64:
* MinGW-w64-gcc must support `-std=g++17`
* MinGW-w64-cmake
* Call cmake with `-G "MinGW Makefiles"`
* Same as above. My mingw prefix is `/usr/x86_64-w64-mingw32/`. If yours is different, you can either symlink that directory to yours or change `engine/CMakeLists.txt`.

## Contributing

This is a personal project aimed at getting familiar with C++, Vulkan, and graphics programming in general, so I'm not looking for contributors at the moment.  
However, if you see something in the code that stinks, I would really appreciate being told why it stinks / what the proper way to do it is. Thanks!  
I'm also always open to questions / talking about the decisions I make with this engine, so hit me up on Discord or in the issues.
