#include "camera.hpp"
#include "light.hpp"
#include "mesh.hpp"
#include "gltf.hpp"
#include "transform.hpp"
#include "image.hpp"

using std::string,
      std::vector,
      std::tuple,
      std::byte;

using namespace vk;

namespace engine {

void MeshRenderSystem(World& w, const VulkanContext& context, uint32_t image_index) 
{
	MeshRenderer* renderer = MeshRenderer::singleton;

	auto tuples = w.get_component_tuples<MeshComponent, Transform3D>();
	glm::vec3 light_translation;
	for (auto& [l, lt] : w.get_component_tuples<LightComponent, Transform3D>()) {
		light_translation = lt.get_translation();
		break;
	}
	glm::mat4 camera_transform;
	glm::mat4 camera_proj;
	for (auto& [c, ct] : w.get_component_tuples<CameraComponent, Transform3D>()){
		camera_transform = ct.get_transform();
		camera_proj = c.get_proj();
		break;
	}
	UniformBufferObject base = {
		glm::mat4(),
		camera_transform,
		camera_proj,
		light_translation
	};

	size_t descriptor_size = sizeof(UniformBufferObject);
	uint32_t min = Renderer::min_uniform_buffer_alignment;
	uint32_t offset = min * uint32_t(ceil(float(descriptor_size) / float(min)));
	vector<byte> buffer;
	buffer.resize(offset * tuples.size());

	for (auto& [mesh, transform] : tuples) {
		int i = mesh.instance;
		base.model = transform.get_transform();
		memmove(&buffer[offset * i], (void*) &base, sizeof(base));
	}

	DeviceMemory memory = renderer->get_uniform_buffer_memory(image_index);
	copy_to_device(context.device, memory, offset * tuples.size(), buffer.data()); 
}

MeshRenderer* MeshRenderer::singleton = nullptr;

void MeshRenderer::allocate_command_buffers(const VulkanContext& context) 
{
	command_buffers = context.device.allocateCommandBuffers(
		{ context.command_pool,
		  CommandBufferLevel::eSecondary,
		  context.images
		}
	);
}

void MeshRenderer::record_command_buffer(const VulkanContext& context, 
                                         vk::CommandBufferInheritanceInfo inheritance_info, 
                                         uint32_t image_index) 
{
		CommandBuffer subbuffer = command_buffers[image_index];
		subbuffer.begin(
			{ CommandBufferUsageFlagBits::eRenderPassContinue 
			  | CommandBufferUsageFlagBits::eSimultaneousUse, 
			  &inheritance_info
			}
		);

		subbuffer.bindPipeline(PipelineBindPoint::eGraphics, pipeline);
		for (auto& mesh : meshes) {
			subbuffer.bindIndexBuffer(mesh.index_buffer.buffer, 0, IndexType::eUint16);
			subbuffer.bindVertexBuffers(
				0, 
				std::array<Buffer, 1>({mesh.vertex_buffer.buffer}), 
				std::array<DeviceSize, 1>({(DeviceSize) 0})
			);
			vector<DescriptorSet> mesh_descriptors = mesh.descriptor_sets[image_index];

			for (int i = 0; i < mesh.instances; i++) {
				subbuffer.bindDescriptorSets(
					PipelineBindPoint::eGraphics, 
					layout, 
					0, 
					mesh_descriptors[i], 
					{}
				); 
				subbuffer.drawIndexed(mesh.index_count, 1, 0, 0, 0);
			}
		}

		subbuffer.end();
}

vector<CommandBuffer> MeshRenderer::get_command_buffers() 
{
	return command_buffers;
}

void MeshRenderer::free_command_buffers(const VulkanContext& context) 
{
	context.device.freeCommandBuffers(context.command_pool, command_buffers);
}

void MeshRenderer::create_pipeline(const VulkanContext& context) 
{
	GraphicsPipelineCreateInfo pipeline_create_info;
	pipeline_create_info.layout = layout;
	pipeline_create_info.stageCount = 2;
	PipelineShaderStageCreateInfo vert_stage (
		{},
		ShaderStageFlagBits::eVertex,
		vert_shader,
		"main",
		nullptr
	);
	PipelineShaderStageCreateInfo frag_stage (
		{},
		ShaderStageFlagBits::eFragment,
		frag_shader,
		"main",
		nullptr
	);
	PipelineShaderStageCreateInfo shader_stages[2] = {vert_stage, frag_stage};
	pipeline_create_info.pStages = shader_stages;

	auto binding_description = vertex::get_vertex_binding_description<vertex::Mesh>();
	auto attribute_descriptions = vertex::Mesh::get_attribute_descriptions();
	PipelineVertexInputStateCreateInfo vertex_input (
		{},
		1,
		&binding_description,
		attribute_descriptions.size(),
		attribute_descriptions.data()
	);
	pipeline_create_info.pVertexInputState = &vertex_input;

	pipeline = create_graphics_pipeline(context, pipeline_create_info);
}

void MeshRenderer::destroy_pipeline(const VulkanContext& context) 
{
	context.device.destroyPipeline(pipeline);
}

void MeshRenderer::create_descriptors(const VulkanContext& context) 
{
	size_t descriptor_size = sizeof(UniformBufferObject);
	uint32_t min = Renderer::min_uniform_buffer_alignment;
	uint32_t offset = min * uint32_t(ceil(float(descriptor_size) / float(min)));

	// TODO: this won't always work, especially for adding instances later.
	uint32_t uniform_buffer_count = 0;

	for (auto& mesh : meshes)
		uniform_buffer_count += mesh.instances;

	DescriptorPoolSize size (DescriptorType::eUniformBuffer, uniform_buffer_count * context.images);
	DescriptorPoolSize sampler_size (DescriptorType::eCombinedImageSampler, uniform_buffer_count * context.images);
	vector<DescriptorPoolSize> sizes = {size, sampler_size};
	descriptor_pool = context.device.createDescriptorPool(
		{ {},
		  uniform_buffer_count * context.images,
		  static_cast<uint32_t>(sizes.size()),
		  sizes.data() 
		}
	);

	vector<DescriptorSetLayout> set_layouts (uniform_buffer_count * context.images, descriptor_set_layout);
	vector<DescriptorSet> descriptor_sets = context.device.allocateDescriptorSets(
		{ descriptor_pool,
		  uniform_buffer_count * context.images,
		  set_layouts.data()
		}
	);

	int t = 0;
	vector<DescriptorImageInfo> image_infos;
	image_infos.resize(uniform_buffer_count * context.images);
	for (int image = 0; image < context.images; image++) {
	for (auto& mesh : meshes) {
		mesh.descriptor_sets.resize(context.images);
		mesh.descriptor_sets[image].resize(mesh.instances);
		for (int i = 0; i < mesh.instances; i++) {
			mesh.descriptor_sets[image][i] = descriptor_sets[t];
			image_infos[t] = DescriptorImageInfo(
				mesh.texture.sampler, 
				mesh.texture.image_view, 
				ImageLayout::eShaderReadOnlyOptimal
			);
			t++;
		}
	}
	}

	uniform_buffers.resize(context.images);
	int total = 0;
	for (auto& uniform_buffer : uniform_buffers) {
		size_t uniform_buffer_size = offset * uniform_buffer_count;
		uniform_buffer = create_uniform_buffer(context, uniform_buffer_size);

		for (int i = 0; i < uniform_buffer_count; i++) {
			DescriptorBufferInfo bi (
				uniform_buffer.buffer,
				i * offset,
				descriptor_size
			);
			WriteDescriptorSet write (
				descriptor_sets[total],
				0,
				0,
				1,
				DescriptorType::eUniformBuffer,
				nullptr,
				&bi,
				nullptr
			);
			WriteDescriptorSet sampler_write (
				descriptor_sets[total],
				1,
				0,
				1,
				DescriptorType::eCombinedImageSampler,
				&image_infos[total],
				nullptr,
				nullptr
			);
			vector<WriteDescriptorSet> writes = {write, sampler_write};
			context.device.updateDescriptorSets(writes.size(), writes.data(), 0, nullptr);
			total++;
		}
	}
}

MeshHandle MeshRenderer::add_mesh(const string& path) 
{
	Renderer* r = Application::get_renderer();
	const VulkanContext& context = r->get_context();

	MeshHandle handle = meshes.size();
	GltfModel model = GltfModel(path);
	DeviceBuffer vertex_buffer = model.create_vertex_buffer(context);
	DeviceBuffer index_buffer = model.create_index_buffer(context);

	meshes.push_back({vertex_buffer, index_buffer, model.get_index_count(), default_texture});

	return handle;
}

TextureHandle MeshRenderer::load_texture(const char* path) 
{
	Renderer* r = Application::get_renderer();
	const VulkanContext& context = r->get_context();

	TextureHandle handle = textures.size();
	SamplerCreateInfo sampler_ci = get_default_sampler_create_info();
	ImageInfo image = load_image(path);
	uint32_t mip_levels = 1;
	if (Renderer::settings.use_mipmapping && linear_filtering_support(context.physical_device, image.format)) {
		mip_levels = get_mip_levels(std::max(image.extent.width, image.extent.height));
		sampler_ci.mipmapMode = SamplerMipmapMode::eLinear;
		sampler_ci.minLod = 0;
		sampler_ci.maxLod = static_cast<float>(mip_levels);
		sampler_ci.mipLodBias = 0;
	}
	TextureCreateInfo texture_ci (
			image.size, 
			image.data,
			image.format,
			image.extent,
			mip_levels,
			context.device.createSampler(sampler_ci));
	Texture tex = create_texture(context, texture_ci);
	textures.push_back(tex);

	return handle;
}

void MeshRenderer::add_instance(World& w, Entity e, MeshHandle handle) 
{
	Mesh& mesh = meshes[handle];
	mesh.instances++;

	w.add_component<MeshComponent>(e, {handle, total_instances});
	w.add_component<Transform3D>(e);
	total_instances++;
}

void MeshRenderer::set_mesh_texture(MeshHandle mesh, TextureHandle texture) 
{
	meshes[mesh].texture = textures[texture];
}

DeviceMemory MeshRenderer::get_uniform_buffer_memory(uint32_t index) 
{
	return uniform_buffers[index].device_memory;
}

void MeshRenderer::cleanup() 
{
	Renderer* r = Application::get_renderer();
	const VulkanContext& context = r->get_context();

	context.device.destroyPipelineLayout(layout);
	context.device.destroyDescriptorSetLayout(descriptor_set_layout);
	context.device.destroyDescriptorPool(descriptor_pool);
	for (auto& uniform_buffer : uniform_buffers)
		uniform_buffer.free(context.device);
	for (auto& mesh : meshes) {
		mesh.vertex_buffer.free(context.device);
		mesh.index_buffer.free(context.device);
	}
	for (auto& texture : textures)
		texture.destroy(context.device);
	context.device.destroyShaderModule(vert_shader);
	context.device.destroyShaderModule(frag_shader);
};

MeshRenderer::MeshRenderer(World& w) 
{
	singleton = this;

	Renderer* r = Application::get_renderer();
	const VulkanContext& context = r->get_context();

	w.add_system(MeshRenderSystem);

	log_info("vert shader");
	//vert_shader = load_shader(context.device, "mesh.vert.glsl.spv");
	vert_shader = load_shader_binary(context.device, &_binary_mesh_vert_glsl_spv_start, &_binary_mesh_vert_glsl_spv_end);
	//frag_shader = load_shader(context.device, "mesh.frag.glsl.spv");
	frag_shader = load_shader_binary(context.device, &_binary_mesh_frag_glsl_spv_start, &_binary_mesh_frag_glsl_spv_end);

	DescriptorSetLayoutBinding descriptor_binding (
		0,
		DescriptorType::eUniformBuffer,
		1,
		ShaderStageFlagBits::eVertex,
		nullptr
	);

	DescriptorSetLayoutBinding sampler_descriptor_binding (
		1,
		DescriptorType::eCombinedImageSampler,
		1,
		ShaderStageFlagBits::eFragment,
		nullptr
	);

	vector<DescriptorSetLayoutBinding> bindings = {descriptor_binding, sampler_descriptor_binding};

	DescriptorSetLayoutCreateInfo descriptor_set_layout_create_info (
		{},
		bindings.size(),
		bindings.data()
	);

	descriptor_set_layout = context.device.createDescriptorSetLayout(descriptor_set_layout_create_info);

	PipelineLayoutCreateInfo layout_create_info (
		{},
		1,
		&descriptor_set_layout,
		0,
		nullptr
	);

	layout = context.device.createPipelineLayout(layout_create_info);

	// create default image
	SamplerCreateInfo sampler_ci = get_default_sampler_create_info();
	vector<uint8_t> image_data (4, 255);
	TextureCreateInfo default_texture_ci (
		sizeof(uint8_t) * image_data.size(), 
		image_data.data(),
		Format::eR8G8B8A8Unorm,
		Extent2D(1,1),
		1,
		context.device.createSampler(sampler_ci)
	);
	default_texture = create_texture(context, default_texture_ci);
	textures.push_back(default_texture);

	r->add_pipeline(this);
	r->add_render_pass_command_buffer(this);
	r->add_descriptor(this);
}
}
