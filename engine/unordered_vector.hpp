#pragma once

#include <utility>
#include <algorithm>
#include <cstring>


class UnorderedVectorException {};
class UnorderedVectorOutOfRangeException : public UnorderedVectorException {};

template <typename T>
class UnorderedVector {


public:
	void add(const T& element);

	void add(T&& element);

	void remove(T& element);

	T pop(T& element);

	void reserve(size_t new_allocation_size);

	void clear() noexcept;

	T* begin() noexcept;
	T* end() noexcept;
	const T* cbegin() const noexcept;
	const T* cend() const noexcept;

	size_t get_size() const noexcept { return size; }
	size_t get_capacity() const noexcept { return capacity; }

	UnorderedVector() noexcept;
	UnorderedVector(size_t initial_allocation);
	UnorderedVector(const UnorderedVector& from);
	UnorderedVector(UnorderedVector&& from) noexcept;
	UnorderedVector& operator=(const UnorderedVector& from);
	UnorderedVector& operator=(UnorderedVector&& from) noexcept;
	~UnorderedVector() noexcept;

private:
	size_t size;
	size_t capacity;
	T* buffer;

	void reallocate();
	bool is_element_in_range(const T* element) const;
	bool is_last_element(const T* element) const;
	void free_elements();
};


template <typename T>
void UnorderedVector<T>::add(const T& element)
{
	if (size == capacity)
		reallocate();

	::new(static_cast<void*>(&buffer[size++])) T(element);
}


template <typename T>
void UnorderedVector<T>::add(T&& element)
{
	if (size == capacity)
		reallocate();

	// call move contsructor on uninitialized memory
	::new(static_cast<void*>(&buffer[size++])) T(std::move(element));
}


template <typename T>
void UnorderedVector<T>::remove(T& element)
{
	if (!is_element_in_range(&element))
		throw UnorderedVectorOutOfRangeException{};

	if (!is_last_element(&element))
		std::swap(element, buffer[size - 1]);

	// call destructor on last element
	buffer[--size].~T();
}


template <typename T>
T UnorderedVector<T>::pop(T& element)
{
	if (!is_element_in_range(&element))
		throw UnorderedVectorOutOfRangeException{};

	if (!is_last_element(&element))
		std::swap(element, buffer[size - 1]);

	return std::move(buffer[--size]);
}


template <typename T>
void UnorderedVector<T>::reserve(size_t new_allocation_size)
{
	if (new_allocation_size < capacity)
		throw UnorderedVectorOutOfRangeException{};

	T* new_buffer = static_cast<T*>(::operator new(sizeof(T) * new_allocation_size));
	std::memcpy(new_buffer, buffer, sizeof(T) * new_allocation_size);
	::operator delete(static_cast<void*>(buffer));
	buffer = new_buffer;
	capacity = new_allocation_size;
}


template <typename T>
void UnorderedVector<T>::clear() noexcept
{
	for (T* p = buffer; p != buffer + size; ++p)
		p->~T();
	size = 0;
	capacity = 0;
}


template <typename T>
T* UnorderedVector<T>::begin() noexcept
{
	return buffer;
}


template <typename T>
const T* UnorderedVector<T>::cbegin() const noexcept
{
	return buffer;
}


template <typename T>
T* UnorderedVector<T>::end() noexcept
{
	return buffer + size;
}


template <typename T>
const T* UnorderedVector<T>::cend() const noexcept
{
	return buffer + size;
}


template <typename T>
UnorderedVector<T>::UnorderedVector() noexcept
	: size{0}, capacity{0}, buffer{nullptr}
{
}


template <typename T>
UnorderedVector<T>::UnorderedVector(size_t initial_allocation)
	: size{0}, capacity{initial_allocation}, buffer{nullptr}
{
	buffer = static_cast<T*>(::operator new(sizeof(T) * capacity));
}


template <typename T>
UnorderedVector<T>::UnorderedVector(const UnorderedVector& from)
	: size{from.size}, capacity{from.capacity}, buffer{nullptr}
{
	buffer = static_cast<T*>(::operator new(sizeof(T) * capacity));
	std::memcpy(buffer, from.buffer, sizeof(T) * size);
}


template <typename T>
UnorderedVector<T>::UnorderedVector(UnorderedVector&& from) noexcept
	: size{0}, capacity{0}, buffer{nullptr}
{
	std::swap(size, from.size);
	std::swap(capacity, from.capacity);
	std::swap(buffer, from.buffer);
}


template <typename T>
UnorderedVector<T>& UnorderedVector<T>::operator=(const UnorderedVector& from)
{
	free_elements();
	size = from.size;
	capacity = from.capacity;
	buffer = static_cast<T*>(::operator new(sizeof(T) * capacity));
	std::memcpy(buffer, from.buffer, sizeof(T) * size);
}


template <typename T>
UnorderedVector<T>& UnorderedVector<T>::operator=(UnorderedVector&& from) noexcept
{
	free_elements();
	size = from.size;
	from.size = 0;
	capacity = from.capacity;
	from.capacity = 0;
	std::swap(buffer, from.buffer);
}


template <typename T>
UnorderedVector<T>::~UnorderedVector() noexcept
{
	free_elements();
}


template <typename T>
void UnorderedVector<T>::reallocate()
{
	size_t new_capacity;
	if (capacity == 0)
		new_capacity = 1;
	else
		new_capacity = capacity << 1;
	T* new_buffer = static_cast<T*>(::operator new(sizeof(T) * new_capacity));
	std::memcpy(new_buffer, buffer, sizeof(T) * size);
	::operator delete(static_cast<void*>(buffer));
	buffer = new_buffer;
	capacity = new_capacity;
}


template <typename T>
bool UnorderedVector<T>::is_element_in_range(const T* element) const
{
	return (element >= buffer) && (element < buffer + size);
}


template <typename T>
bool UnorderedVector<T>::is_last_element(const T* element) const
{
	return element == buffer + size - 1;
}


template <typename T>
void UnorderedVector<T>::free_elements()
{
	for (T* p = buffer; p != buffer + size; ++p)
		p->~T();

	::operator delete(static_cast<void*>(buffer));
	buffer = nullptr;
}
