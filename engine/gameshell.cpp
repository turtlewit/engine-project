#include <tuple>
#include <stdexcept>
#include <iostream>
#include <fstream>

#include <SDL2/SDL.h>

#include "gameshell.hpp"
#include "vars.cpp"
#include "logger.hpp"
#include "application.hpp"

#include "sdl_keycode_strings.hpp"

using std::string,
      std::unordered_map,
      std::vector,
      std::tuple,
      std::make_tuple,
      std::tie;

namespace engine::gsh {
static unordered_map<string, Verb*> verbs;
static vector<string> command_buffer;

unordered_map<string, Verb*>& get_verb_list() 
{
	return verbs;
}

void add_verb(string name, Verb* verb) 
{
	verbs[name] = verb;
}

void queue_command(string command) 
{
	command_buffer.push_back(command);
}

tuple<string, vector<string>> split_command(string command) 
{
	string verb_string;
	vector<string> arguments;

	int i;
	for (i = 0; i < command.size(); i++) {
		char c = command[i];
		if (c == ' ') 
			break;
	}
	verb_string = command.substr(0, i);

	bool whitespace = true;
	int start = i;
	for (i = i; i < command.size(); i++) {
		char c = command[i];
		if (whitespace && c != ' ') {
			whitespace = false;
			start = i;
		} else if (!whitespace && c == ' ') {
			whitespace = true;
			arguments.push_back(command.substr(start, i - start));
		}
	}

	if (!whitespace)
		arguments.push_back(command.substr(start));

	return make_tuple(verb_string, arguments);
}

void remove_comment(string& command)
{
	size_t comment_location = command.find_first_of('#');
	if (comment_location != string::npos) {
		command = command.substr(0, comment_location);
	}
}

void remove_whitespace(string& command)
{
	// before
	int i = 0;
	for (auto& c : command) {
		if (!isspace(c))
			break;
		i++;
	}
	// after
	int j = command.size();
	for (j; j > 0; j--) {
		if (!isspace(command[j-1]))
			break;
	}
	if (i > j)
		command = "";
	command = command.substr(i, j);
}

void execute_command(string command) 
{
	remove_comment(command);
	remove_whitespace(command);
	if (command.size() == 0)
		return;
	string verb_string;
	CommandInfo info;
	tie(verb_string, info.arguments) = split_command(command);

	Verb* verb;
	try {
		verb = verbs.at(verb_string);
	} catch (const std::out_of_range& e) {
		log_error("Command not found: %s. Full command: %s", verb_string.c_str(), command.c_str());
		return;
	}
	
	try {
		verb->function(info);
	} catch (const std::invalid_argument& e) {
		log_error("Failed to execute command %s: \n\t%s", verb_string.c_str(), e.what());
	}
}

void execute_commands() 
{
	for (string& command : command_buffer) {
		execute_command(command);
	}

	command_buffer.clear();
	command_buffer.reserve(64);
}

struct Echo : Verb {
	void function(CommandInfo info) override 
	{
		string m = "";
		for (string& arg : info.arguments) {
			if (arg[0] == '$')
				m += get_var_raw(arg.substr(1), "") + " ";
			else if (arg[0] == '{' and arg.back() == '}')
				m += get_var_raw(arg.substr(1, arg.size() - 2), "") + " ";
			else
				m += arg + " ";
		}

		// remove end space
		if (m.size() > 0) 
			m.pop_back();

		//log_info(m);
		std::cout << m << std::endl;
	}
};

struct Exec : Verb {
	void function(CommandInfo info) override
	{
		string path = info.arguments[0];
		for (int i = 1; i < info.arguments.size(); i++)
			path += string(" ") + info.arguments[i];

		std::ifstream file(path.c_str());

		if (!file.is_open()) {
			char buffer[256];
			sprintf(buffer, "Could not open requested script %s", path.c_str());
			throw std::runtime_error(buffer);
		}
		vector<string> lines;
		lines.resize(1);
		while (std::getline(file, lines.back())) {
			lines.resize(lines.size() + 1);
		}
		lines.pop_back();
		file.close();

		for (auto& c : lines) {
			if (c.size() > 0)
				execute_command(c);
		}
	}
};

struct SetKey : Verb {
	void function(CommandInfo info) override
	{
		string& key = info.arguments[0];
		for (int i = 1; i < info.arguments.size(); i++) {
			SDL_Keycode keycode;
			try {
				keycode = keycode_map.at(info.arguments[i]);
			} catch (std::out_of_range) {
				log_error("Key %s not recognized.", info.arguments[i].c_str());
				continue;
			}
			if (keycode == SDLK_UNKNOWN) {
				log_error("Key %s not recognized.", info.arguments[i].c_str());
				continue;
			}
			Application::input.register_action(key, keycode);
		}
	}
};

void add_default_verbs() 
{
	add_verb("echo", new Echo());
	add_verb("exec", new Exec());
	add_verb("set", new SetVerb());
	add_verb("setkey", new SetKey());
}

void init() 
{
	command_buffer.reserve(64);
	add_default_verbs();
}

void cleanup() 
{
	for (auto [k, v] : verbs)
		delete v;
}
}
