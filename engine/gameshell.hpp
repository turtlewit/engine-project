#pragma once

#include <string>
#include <unordered_map>
#include <vector>

namespace engine::gsh {
struct CommandInfo {
	std::vector<std::string> arguments;
};

struct Verb {
	virtual void function(CommandInfo info) = 0;
};

void add_verb(std::string, Verb* verb);
void queue_command(std::string command);
void execute_command(std::string command);
void execute_commands();
std::unordered_map<std::string, Verb*>& get_verbs();
void init();
void cleanup();
}
