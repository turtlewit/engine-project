#pragma once

#include <vulkan/vulkan.hpp>
#include <functional>

#include "vulkan_context.hpp"
#include "application.hpp"
#include "glm/glm.hpp"

class SDL_Window;

extern const char _binary_copy_frag_glsl_spv_start;
extern const char _binary_copy_frag_glsl_spv_end;

extern const char _binary_fxaa_frag_glsl_spv_start;
extern const char _binary_fxaa_frag_glsl_spv_end;

namespace engine {
class World;
class PostProcessRenderer;

struct RendererSettings {
	bool use_display_timing = false;
	bool use_anisotropy = false;
	int max_anisotropy = 1;
	bool use_mipmapping = false;
	bool use_fxaa = false;
	int device_index = 0;
};

namespace vertex {
template<typename T>
vk::VertexInputBindingDescription get_vertex_binding_description (uint32_t binding = 0) 
{
	return {binding, sizeof(T), vk::VertexInputRate::eVertex};
}

struct Quad {
	float uv[2];

	static std::array<vk::VertexInputAttributeDescription, 1> get_attribute_descriptions() 
	{
		std::array<vk::VertexInputAttributeDescription, 1> ad;
		ad[0] = {0, 0, vk::Format::eR32G32Sfloat, offsetof(Quad, uv)};
		return ad;
	}
};

struct Mesh {
	glm::vec3 position;
	glm::vec3 normal;
	glm::vec2 uv;

	static std::array<vk::VertexInputAttributeDescription, 3> get_attribute_descriptions() 
	{
		std::array<vk::VertexInputAttributeDescription, 3> ad = {{
			{0, 0, vk::Format::eR32G32B32Sfloat, 0},
			{1, 0, vk::Format::eR32G32B32Sfloat, offsetof(Mesh, normal)},
			{2, 0, vk::Format::eR32G32Sfloat, offsetof(Mesh, uv)} 
		}};
		return ad;
	}
};
}

struct Quad {
	std::array<vertex::Quad, 4> vertices;
	std::array<uint16_t, 6> indices;
	Quad() 
	{
		vertices = {{
			{0.f, 0.f},
			{1.f, 0.f},
			{1.f, 1.f},
			{0.f, 1.f}
		}};

		indices = {{
			0, 1, 2,
			0, 2, 3 
		}};
	}
};

struct Texture {
	vk::Image image;
	vk::DeviceMemory memory;
	vk::ImageView image_view;
	vk::Sampler sampler;

	void destroy(vk::Device device) 
	{
		device.freeMemory(memory);
		device.destroyImageView(image_view);
		device.destroyImage(image);
		device.destroySampler(sampler);
	}
};

// thanks vulkan.hpp ;3
struct TextureCreateInfo {
	TextureCreateInfo(
			size_t buffer_size_ = 0, 
			void* image_data_ = nullptr, 
			vk::Format image_format_ = vk::Format(), 
			vk::Extent2D extent_ = vk::Extent2D(), 
			uint32_t mip_levels_ = 1,
			vk::Sampler sampler_ = vk::Sampler()) 
		: buffer_size( buffer_size_ )
		, image_data( image_data_ )
		, image_format( image_format_ )
		, extent( extent_ )
		, mip_levels( mip_levels_)
		, sampler( sampler_ ) 
	{}

	size_t buffer_size;
	void* image_data;
	vk::Format image_format;
	vk::Extent2D extent;
	uint32_t mip_levels;
	vk::Sampler sampler;
};

class RenderPipeline {
	protected:
	vk::PipelineLayout layout;
	vk::Pipeline pipeline;

	public:
	virtual void create_pipeline(const VulkanContext& context) = 0;
	virtual void destroy_pipeline(const VulkanContext& context) = 0;
};

class RenderPassCommandBuffer {
	public:
	virtual void allocate_command_buffers(const VulkanContext& context) = 0;
	virtual void record_command_buffer(const VulkanContext& context, vk::CommandBufferInheritanceInfo inheritance_info, uint32_t image_index) = 0;
	virtual std::vector<vk::CommandBuffer> get_command_buffers() = 0;
	virtual void free_command_buffers(const VulkanContext& context) = 0;
};

class PostProcessCommandBuffer : public RenderPassCommandBuffer {};

class Descriptor {
	public:
	virtual void create_descriptors(const VulkanContext& context) = 0;
};

class PostProcessDescriptor {
	public:
	virtual void create_descriptors(const VulkanContext& context, std::vector<Texture> color_attachments) = 0;
	virtual void destroy_descriptors(const VulkanContext& context) = 0;
};

struct DeviceBuffer {
	vk::Buffer buffer;
	vk::DeviceMemory device_memory;

	void free(vk::Device device) 
	{
		device.destroyBuffer(buffer);
		device.freeMemory(device_memory);
	}
};

void copy_to_device(vk::Device device, 
                    vk::DeviceMemory dst_memory, 
                    size_t src_size, 
                    void* src_memory, 
                    vk::DeviceSize dst_offset = 0, 
                    vk::DeviceSize dst_size = VK_WHOLE_SIZE);

vk::ShaderModule load_shader(vk::Device, const char* name);
vk::ShaderModule load_shader_binary(vk::Device device, const char* start, const char* end);

vk::Pipeline create_graphics_pipeline(const VulkanContext& context, vk::GraphicsPipelineCreateInfo& ci);
uint32_t find_memory_type(vk::PhysicalDevice physical_device, vk::MemoryPropertyFlags properties, uint32_t typeFilter);
DeviceBuffer create_device_buffer(const VulkanContext& context, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, vk::DeviceSize buffer_size);
DeviceBuffer create_uniform_buffer(const VulkanContext& context, size_t buffer_size);
void copy_buffer(const VulkanContext& context, vk::Buffer src, vk::Buffer dst, vk::DeviceSize size);
DeviceBuffer create_vertex_buffer(const VulkanContext& context, size_t buffer_size, void* vertex_data);
DeviceBuffer create_index_buffer(const VulkanContext& context, size_t buffer_size, void* index_data);

vk::Image create_image(vk::Device device, vk::Format image_format, vk::Extent2D extent, vk::ImageTiling tiling, vk::ImageUsageFlags image_usage, uint32_t queue_family, vk::ImageLayout layout);
vk::DeviceMemory allocate_image_memory(const VulkanContext& context, vk::Image image, vk::MemoryPropertyFlags props = vk::MemoryPropertyFlagBits::eDeviceLocal);
vk::ImageView create_image_view(vk::Device device, vk::Image image, vk::Format format, vk::ImageAspectFlags aspect);
vk::SamplerCreateInfo get_default_sampler_create_info();
void copy_buffer_to_image(const VulkanContext& context, vk::Buffer src, vk::Image dst, vk::Extent2D extent);
bool linear_filtering_support(vk::PhysicalDevice physical_device, vk::Format format);
void generate_mipmaps(const VulkanContext& context, vk::Format format, vk::Image image, vk::Extent2D extent, uint32_t mip_levels);
Texture create_texture(const VulkanContext& context, TextureCreateInfo ci);
Texture create_device_local_texture(const VulkanContext& context, TextureCreateInfo ci, vk::ImageUsageFlags image_usage, vk::ImageLayout desired_layout);
void transition_image_layout(const VulkanContext& context, vk::Image image, vk::Format format, vk::ImageLayout old_layout, vk::ImageLayout new_layout, uint32_t mip_levels);
uint32_t get_mip_levels(uint32_t res);

vk::CommandBuffer begin_single_use_command_buffer(const VulkanContext& context);
void end_and_submit(const VulkanContext& context, vk::CommandBuffer command_buffer);

class Renderer {
	private:
	const uint32_t window_flags;

	SDL_Window* window;
	ApplicationData appdata;

	VulkanContext context;

	vk::Instance instance;
	vk::SurfaceKHR surface;

	vk::PhysicalDevice physical_device;
	vk::Device device;

	vk::DispatchLoaderDynamic dl;

	QueueFamilies queue_families;

	vk::Queue graphics_queue;
	vk::Queue present_queue;

	vk::SwapchainKHR swapchain;
	vk::Extent2D swapchain_extent;
	vk::Format surface_format;
	std::vector<vk::Image> swapchain_images;
	std::vector<vk::ImageView> swapchain_image_views;
	std::vector<Texture> color_attachments;
	vk::RenderPass render_pass;
	vk::RenderPass post_process_render_pass;
	std::vector<vk::Framebuffer> framebuffers;
	std::vector<vk::Framebuffer> post_process_framebuffers;
	vk::CommandPool command_pool;
	std::vector<vk::CommandBuffer> command_buffers;

	bool assets_changed = true;
	std::vector<RenderPipeline*> pipelines_to_create;
	std::vector<RenderPipeline*> created_pipelines;
	std::vector<RenderPassCommandBuffer*> command_buffers_to_record;
	std::vector<RenderPassCommandBuffer*> recorded_command_buffers;
	std::vector<PostProcessCommandBuffer*> pp_command_buffers_to_record;
	std::vector<PostProcessCommandBuffer*> recorded_pp_command_buffers;
	std::vector<Descriptor*> descriptors_to_create;
	std::vector<Descriptor*> created_descriptors;
	std::vector<PostProcessDescriptor*> pp_descriptors_to_create;
	std::vector<PostProcessDescriptor*> created_pp_descriptors;

	vk::Semaphore image_available;
	vk::Semaphore render_finished;

	vk::Image depth_image;
	vk::DeviceMemory depth_image_memory;
	vk::ImageView depth_image_view;

	PostProcessRenderer* copy_shader;
	PostProcessRenderer* fxaa_renderer;

	uint32_t present_id = 0;

	void init_pass(vk::SwapchainKHR old_swapchain = nullptr);
	void create_descriptors();
	void destroy_descriptors();
	void create_pipelines();
	void destroy_pipelines();
	void record_render_pass_command_buffers();
	void free_render_pass_command_buffers();
	void create_new_assets();
	void destroy_pass();
	void reset_swapchain();

	public:
	bool* running;
	bool enable_display_timing = false;
	static RendererSettings settings;

	static uint32_t min_uniform_buffer_alignment;

	void init();

	uint32_t draw_frame(uint64_t scheduled_present_time, World& w);
	void print_frame_info();
	void wait_idle();
	void resize(World& w);
	void load_settings();
	void add_pipeline(RenderPipeline* p);
	void add_render_pass_command_buffer(RenderPassCommandBuffer* cb);
	void add_post_process_command_buffer(PostProcessCommandBuffer* cb);
	void add_descriptor(Descriptor* d);
	void add_post_process_descriptor(PostProcessDescriptor* d);

	uint64_t get_refresh_cycle_duration();
	uint64_t get_next_present_time(uint64_t desired_delta);
	const VulkanContext& get_context();

	Renderer(ApplicationData d);
	~Renderer();
};
}
