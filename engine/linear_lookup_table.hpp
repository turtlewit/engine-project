#pragma once

#include "unordered_vector.hpp"

class LinearLookupTableException {};
class LinearLookupTableOutOfRangeException : public LinearLookupTableException {};

template <typename K, typename V>
class LinearLookupTable {
public:
	void add(const K& key, const V& value);
	void add(K&& key, V&& value);

	void remove(const K& key);

	V pop_value(const K& key);

	const V& operator[](const K& key) const;
	V& operator[](const K& key);

	LinearLookupTable() = default;
	LinearLookupTable(size_t initial_allocation);
	LinearLookupTable(const LinearLookupTable& other) = default;
	LinearLookupTable(LinearLookupTable&& other) = default;

private:
	size_t get_index(const K& key);
	UnorderedVector<K> keys;
	UnorderedVector<V> values;
};


template <typename K, typename V>
void LinearLookupTable<K,V>::add(const K& key, const V& value)
{
	keys.add(key);
	values.add(value);
}


template <typename K, typename V>
void LinearLookupTable<K,V>::add(K&& key, V&& value)
{
	keys.add(std::move(key));
	values.add(std::move(value));
}


template <typename K, typename V>
void LinearLookupTable<K,V>::remove(const K& key)
{
	size_t index = get_index(key);
	try {
		keys.remove(*(keys.begin() + index));
		values.remove(*(values.begin() + index));
	} catch (UnorderedVectorOutOfRangeException) {
		throw LinearLookupTableOutOfRangeException{};
	}
}


template <typename K, typename V>
V LinearLookupTable<K,V>::pop_value(const K& key)
{
	size_t index = get_index(key);
	keys.remove(*(keys.begin() + index));
	return values.pop(*(values.begin() + index));
}


template <typename K, typename V>
const V& LinearLookupTable<K,V>::operator[](const K& key) const
{
	size_t index = get_index(key);
	return *(values.begin() + index);
}


template <typename K, typename V>
V& LinearLookupTable<K,V>::operator[](const K& key)
{
	size_t index;
	try {
		index = get_index(key);
	} catch (LinearLookupTableOutOfRangeException) {
		keys.add(key);
		values.add(V());
		return *(values.end() - 1);
	}
	return *(values.begin() + index);
}


template <typename K, typename V>
LinearLookupTable<K,V>::LinearLookupTable(size_t initial_allocation)
	: keys{UnorderedVector<K>(initial_allocation)}, values{UnorderedVector<V>(initial_allocation)}
{
}


template <typename K, typename V>
size_t LinearLookupTable<K,V>::get_index(const K& key)
{
	for (const auto& other_key : keys)
		if (key == other_key)
			return static_cast<size_t>(&other_key - keys.begin());
	throw LinearLookupTableOutOfRangeException{};
}
