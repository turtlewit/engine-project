#pragma once
#include <iostream>

namespace engine {

struct BitSet {
	BitSet()
		: l3{0}
	{
	}

	void add(uint32_t i3)
	{
		size_t v3 = 1 << i3;
		l3 |= v3;
	}

	void remove(uint32_t i3)
	{
		size_t v3 = ~(1 << i3);
		l3 &= v3;
	}

	bool contains(uint32_t i3) const
	{
		size_t v3 = 1 << i3;
		return (l3 & v3) != 0;
	}
	size_t l3;
};

} // namespace engine
