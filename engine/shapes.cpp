#include <string>

#include <glm/glm.hpp>
#include <glm/vec2.hpp>

#include "renderer.hpp"
#include "application.hpp"
#include "shapes.hpp"
#include "world.hpp"
#include "system.hpp"
#include "transform.hpp"
#include "logger.hpp"

using namespace vk;
using std::string,
      std::vector,
      std::byte,
      std::tuple;

namespace engine {

void CircleSystem(World& w, const VulkanContext& context, uint32_t image_index)
{
	CircleRenderer* circle_renderer = CircleRenderer::singleton;

	auto tuples = w.get_component_tuples<CircleComponent, Transform2D>();
	size_t descriptor_size = sizeof(ScreenPosition);
	uint32_t min = Renderer::min_uniform_buffer_alignment;
	uint32_t offset = min * uint32_t(ceil(float(descriptor_size) / float(min)));
	vector<byte> buffer;
	buffer.resize(offset * tuples.size());
	
	ScreenPosition base = ScreenPosition();

	uint32_t screen_size[2] = {context.swapchain_image_extent.width, context.swapchain_image_extent.height};
	auto extent = glm::uvec2(screen_size[0], screen_size[1]);
	int i = 0;
	for (auto& [c, t] : tuples) {
		base.radius = c.radius;
		base.extent = extent;
		base.pos.x = (int) t.x;
		base.pos.y = (int) t.y;
		memmove(&buffer[i * offset], (void*) &base, sizeof(base));
		i++;
	}
	DeviceMemory memory = circle_renderer->get_uniform_buffer_memory(image_index);
	copy_to_device(context.device, memory, buffer.size(), buffer.data()); 
}

void CircleRenderer::allocate_command_buffers(const VulkanContext& context) 
{
	command_buffers = context.device.allocateCommandBuffers(
		{ context.command_pool,
		  CommandBufferLevel::eSecondary,
		  context.images
		}
	);
}

CircleRenderer* CircleRenderer::singleton = nullptr;

void CircleRenderer::record_command_buffer(const VulkanContext& context, 
                                           vk::CommandBufferInheritanceInfo inheritance_info, 
                                           uint32_t image_index) 
{
		CommandBuffer subbuffer = command_buffers[image_index];
		subbuffer.begin(
			{ CommandBufferUsageFlagBits::eRenderPassContinue 
			  | CommandBufferUsageFlagBits::eSimultaneousUse, 
			  &inheritance_info
			}
		);

		subbuffer.bindPipeline(PipelineBindPoint::eGraphics, pipeline);
		subbuffer.bindIndexBuffer(index_buffer.buffer, 0, IndexType::eUint16);
		subbuffer.bindVertexBuffers(
			0, 
			std::array<Buffer, 1>({vertex_buffer.buffer}), 
			std::array<DeviceSize, 1>({(DeviceSize) 0})
		);

		for (int i = 0; i < colors.size(); i++) {
			int descriptor_index = (colors.size() * image_index) + i;
			subbuffer.bindDescriptorSets(PipelineBindPoint::eGraphics, layout, 0, descriptor_sets[descriptor_index], {}); 
			subbuffer.pushConstants(
				layout, 
				ShaderStageFlagBits::eFragment, 
				0, 
				sizeof(Color), 
				(void*) &colors[i].r
			);
			subbuffer.drawIndexed(quad.indices.size(), 1, 0, 0, 0);
			descriptor_index += context.images;
		}
		subbuffer.end();
}

vector<CommandBuffer> CircleRenderer::get_command_buffers() 
{
	return command_buffers;
}

void CircleRenderer::free_command_buffers(const VulkanContext& context) 
{
	context.device.freeCommandBuffers(context.command_pool, command_buffers);
}

void CircleRenderer::create_pipeline(const VulkanContext& context) 
{
	GraphicsPipelineCreateInfo pipeline_create_info;
	pipeline_create_info.layout = layout;
	pipeline_create_info.stageCount = 2;
	PipelineShaderStageCreateInfo vert_stage (
		{},
		ShaderStageFlagBits::eVertex,
		vert_shader,
		"main",
		nullptr
	);
	PipelineShaderStageCreateInfo frag_stage (
		{},
		ShaderStageFlagBits::eFragment,
		frag_shader,
		"main",
		nullptr
	);
	PipelineShaderStageCreateInfo shader_stages[2] = {vert_stage, frag_stage};
	pipeline_create_info.pStages = shader_stages;

	auto binding_description = vertex::get_vertex_binding_description<vertex::Quad>();
	auto attribute_descriptions = vertex::Quad::get_attribute_descriptions();
	PipelineVertexInputStateCreateInfo vertex_input (
		{},
		1,
		&binding_description,
		attribute_descriptions.size(),
		attribute_descriptions.data()
	);
	pipeline_create_info.pVertexInputState = &vertex_input;

	PipelineRasterizationStateCreateInfo rasterization_state (
		{},
		VK_FALSE, // depth clamp
		VK_FALSE, // rasterizer discard
		PolygonMode::eFill,
		CullModeFlagBits::eBack,
		FrontFace::eClockwise,
		VK_FALSE, // depth bias
		0.f, // depth bias constant factor
		0.f, // depth bias clamp
		0.f, // depth bias slope factor
		1.f // line width
	);
	pipeline_create_info.pRasterizationState = &rasterization_state;

	PipelineDepthStencilStateCreateInfo depth_stencil (
		{},
		false,
		false,
		CompareOp::eLess,
		false,
		false
	);
	pipeline_create_info.pDepthStencilState = &depth_stencil;

	pipeline = create_graphics_pipeline(context, pipeline_create_info);
}

void CircleRenderer::destroy_pipeline(const VulkanContext& context) 
{
	context.device.destroyPipeline(pipeline);
}

void CircleRenderer::create_descriptors(const VulkanContext& context) 
{
	size_t descriptor_size = sizeof(ScreenPosition);
	uint32_t min = Renderer::min_uniform_buffer_alignment;
	uint32_t offset = min * uint32_t(ceil(float(descriptor_size) / float(min)));

	// TODO: this won't always work, especially for adding instances later.
	uint32_t uniform_buffer_count = colors.size() * context.images;
	DescriptorPoolSize size (DescriptorType::eUniformBuffer, uniform_buffer_count);
	descriptor_pool = context.device.createDescriptorPool(
		{ {},
		  uniform_buffer_count,
		  1,
		  &size
		}
	);

	vector<DescriptorSetLayout> set_layouts (uniform_buffer_count, descriptor_set_layout);
	descriptor_sets = context.device.allocateDescriptorSets(
		{ descriptor_pool,
		  uniform_buffer_count,
		  set_layouts.data()
		}
	);

	uniform_buffers.resize(context.images);
	int total = 0;
	for (auto& uniform_buffer : uniform_buffers) {
		size_t uniform_buffer_size = offset * colors.size();
		size_t descriptor_size = sizeof(ScreenPosition);
		uniform_buffer = create_uniform_buffer(context, uniform_buffer_size);

		for (int i = 0; i < colors.size(); i++) {
			DescriptorBufferInfo bi (
				uniform_buffer.buffer,
				i * offset,
				descriptor_size
			);
			WriteDescriptorSet write (
				descriptor_sets[total],
				0,
				0,
				1,
				DescriptorType::eUniformBuffer,
				nullptr,
				&bi,
				nullptr
			);
			context.device.updateDescriptorSets(1, &write, 0, nullptr);
			total++;
		}
	}
}

void CircleRenderer::add_circle(World& w, Entity e, unsigned int radius, Color color) 
{
	CircleComponent c = {radius};
	w.add_component<CircleComponent>(e, c);
	w.add_component<Transform2D>(e, Transform2D());
	colors.push_back(color);
}

DeviceMemory CircleRenderer::get_uniform_buffer_memory(uint32_t index) 
{
	return uniform_buffers[index].device_memory;
}

void CircleRenderer::cleanup() 
{
	Renderer* r = Application::get_renderer();
	const VulkanContext& context = r->get_context();
	context.device.destroyPipelineLayout(layout);
	context.device.destroyDescriptorSetLayout(descriptor_set_layout);
	context.device.destroyDescriptorPool(descriptor_pool);
	for (auto& uniform_buffer : uniform_buffers)
		uniform_buffer.free(context.device);
	vertex_buffer.free(context.device);
	index_buffer.free(context.device);
	context.device.destroyShaderModule(vert_shader);
	context.device.destroyShaderModule(frag_shader);
}

CircleRenderer::CircleRenderer(World& w) 
{
	singleton = this;

	Renderer* r = Application::get_renderer();
	const VulkanContext& context = r->get_context();

	quad = Quad();
	vertex_buffer = create_vertex_buffer(context, sizeof(vertex::Quad) * quad.vertices.size(), (void*) quad.vertices.data());
	index_buffer = create_index_buffer(context, sizeof(uint16_t) * quad.indices.size(), (void*) quad.indices.data());

	w.add_system(CircleSystem);

	vert_shader = load_shader_binary(context.device, &_binary_circle_vert_glsl_spv_start, &_binary_circle_vert_glsl_spv_end);
	frag_shader = load_shader_binary(context.device, &_binary_circle_frag_glsl_spv_start, &_binary_circle_frag_glsl_spv_end);

	PushConstantRange push_constant_range (
		ShaderStageFlagBits::eFragment,
		0,
		sizeof(Color)
	);

	DescriptorSetLayoutBinding descriptor_binding (
		0,
		DescriptorType::eUniformBuffer,
		1,
		ShaderStageFlagBits::eVertex,
		nullptr
	);

	DescriptorSetLayoutCreateInfo descriptor_set_layout_create_info (
		{},
		1,
		&descriptor_binding
	);

	descriptor_set_layout = context.device.createDescriptorSetLayout(descriptor_set_layout_create_info);

	PipelineLayoutCreateInfo layout_create_info (
		{},
		1,
		&descriptor_set_layout,
		1,
		&push_constant_range
	);

	layout = context.device.createPipelineLayout(layout_create_info);

	r->add_pipeline(this);
	r->add_render_pass_command_buffer(this);
	r->add_descriptor(this);
}
}
