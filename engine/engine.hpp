#pragma once

namespace engine {

class Application;
class World; 

class GameState {
	public:
	virtual void init(World& world) = 0;
	virtual void cleanup() = 0;
	virtual ~GameState() 
	{
	}
};
}
