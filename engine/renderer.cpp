#include <vector>
#include <stdexcept>
#include <fstream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_vulkan.h>

#include "world.hpp"
#include "renderer.hpp"
#include "logger.hpp"
#include "system.hpp"
#include "camera.hpp"
#include "vars.hpp"
#include "post_process.hpp"

using namespace vk;
using std::vector,
      std::max,
      std::min,
      std::array,
      std::runtime_error,
      std::numeric_limits;
using engine::gsh::get_var;

namespace engine {

uint32_t Renderer::min_uniform_buffer_alignment = 0;
RendererSettings Renderer::settings;

// General Rendering Functions
void copy_to_device(Device device, 
                    DeviceMemory dst_memory, 
                    size_t src_size,
                    void* src_memory, 
                    DeviceSize dst_offset, 
                    DeviceSize dst_size) 
{
	void* data = device.mapMemory(dst_memory, dst_offset, dst_size);
	memcpy(data, src_memory, src_size);
	device.unmapMemory(dst_memory);
}

ShaderModule load_shader(Device device, const char* name) 
{
	std::ifstream file(name, std::ios::ate | std::ios::binary);

	if (!file.is_open()) {
		log_error("%s", name);
		throw runtime_error("Could not open file!");
	}

	size_t file_size = (size_t) file.tellg();
	log_info("file size %d", file_size);

	vector<char> buffer(file_size);

	file.seekg(0);
	file.read(buffer.data(), file_size);
	file.close();

	return device.createShaderModule(
		{ {}, //flags
		  file_size,
		  (uint32_t*) buffer.data()
		}
	);
}

ShaderModule load_shader_binary(Device device, const char* start, const char* end)
{
	size_t size = reinterpret_cast<size_t>(end) - reinterpret_cast<size_t>(start);
	log_info("file size %d", size);
	return device.createShaderModule(
		{ {}, //flags
		  (size_t) size,
		  (uint32_t*) start
		}
	);
}

Pipeline create_graphics_pipeline(const VulkanContext& context, GraphicsPipelineCreateInfo& ci) 
{
	PipelineInputAssemblyStateCreateInfo input_assembly (
		{},
		PrimitiveTopology::eTriangleList,
		VK_FALSE
	);
	if (ci.pInputAssemblyState == nullptr)
		ci.pInputAssemblyState = &input_assembly;

	Viewport viewport (
		0.f, 0.f,
		(float) context.swapchain_image_extent.width, (float) context.swapchain_image_extent.height,
		0.f, 1.f
	);
	Rect2D scissor (
		{0, 0},
		context.swapchain_image_extent
	);
	PipelineViewportStateCreateInfo viewport_state (
		{},
		1,
		&viewport,
		1,
		&scissor
	);
	if (ci.pViewportState == nullptr)
		ci.pViewportState = &viewport_state;

	PipelineRasterizationStateCreateInfo rasterization_state (
		{},
		VK_FALSE, // depth clamp
		VK_FALSE, // rasterizer discard
		PolygonMode::eFill,
		CullModeFlagBits::eBack,
		FrontFace::eCounterClockwise,
		VK_FALSE, // depth bias
		0.f, // depth bias constant factor
		0.f, // depth bias clamp
		0.f, // depth bias slope factor
		1.f // line width
	);
	if (ci.pRasterizationState == nullptr)
		ci.pRasterizationState = &rasterization_state;
	
	PipelineMultisampleStateCreateInfo multisample_state (
		{},
		SampleCountFlagBits::e1,
		VK_FALSE, // sample shading
		1.f, // min sample shading
		nullptr, // sample mask
		VK_FALSE, // alpha to coverage 
		VK_FALSE // alpha to one 
	);
	if (ci.pMultisampleState == nullptr)
		ci.pMultisampleState = &multisample_state;

	PipelineColorBlendAttachmentState color_blending_attachment (
		VK_TRUE,
		BlendFactor::eSrcAlpha,
		BlendFactor::eOneMinusSrcAlpha,
		BlendOp::eAdd,
		BlendFactor::eOne,
		BlendFactor::eZero,
		BlendOp::eAdd,
		ColorComponentFlagBits::eR
		| ColorComponentFlagBits::eG
		| ColorComponentFlagBits::eB
		| ColorComponentFlagBits::eA
	);

	PipelineColorBlendStateCreateInfo color_blending (
		{},
		VK_FALSE,
		LogicOp::eCopy,
		1,
		&color_blending_attachment,
		{0.f, 0.f, 0.f, 0.f}
	);
	if (ci.pColorBlendState == nullptr)
		ci.pColorBlendState = &color_blending;

	PipelineDepthStencilStateCreateInfo depth_stencil (
		{},
		true,
		true,
		CompareOp::eLess,
		false,
		false
	);
	if (ci.pDepthStencilState == nullptr)
		ci.pDepthStencilState = &depth_stencil;

	if (ci.renderPass == RenderPass())
		ci.renderPass = context.render_pass;

	vector<GraphicsPipelineCreateInfo> cis ({ci});
	return context.device.createGraphicsPipelines(
		PipelineCache(),
		cis
	)[0];
}

uint32_t find_memory_type(PhysicalDevice physical_device, MemoryPropertyFlags properties, uint32_t typeFilter) 
{
	PhysicalDeviceMemoryProperties memp = physical_device.getMemoryProperties();
	bool found = false;
	int type_index;
	for (int i = 0; i < memp.memoryTypeCount; i++) {
		if ((typeFilter & (1 << i)) && ((memp.memoryTypes[i].propertyFlags & properties) == properties)) {
			type_index = i;
			found = true;
			break;
		}
	}

	if (found)
		return uint32_t(type_index);

	throw std::runtime_error("Could not find type index with given properties!");
}

DeviceBuffer create_device_buffer(const VulkanContext& context, 
                                  BufferUsageFlags usage, 
				  MemoryPropertyFlags properties, 
				  DeviceSize buffer_size) 
{
	Buffer b = context.device.createBuffer(
		{ {},
		  (DeviceSize) buffer_size,
		  usage,
		  SharingMode::eExclusive,
		  1,
		  &context.queue_families.graphics // TODO: transfer queue?
		}
	);

	auto memr = context.device.getBufferMemoryRequirements(b);
	uint32_t memory_type_index;
	try {
		memory_type_index = find_memory_type(context.physical_device, properties, memr.memoryTypeBits);
	} catch (const std::runtime_error& e) {
		context.device.destroyBuffer(b);
		throw e;
	}

	DeviceMemory buffer_memory = context.device.allocateMemory(
		{ memr.size,
		  memory_type_index
		}
	);

	context.device.bindBufferMemory(b, buffer_memory, 0);

	return DeviceBuffer {b, buffer_memory};
}

DeviceBuffer create_uniform_buffer(const VulkanContext& context, size_t buffer_size) 
{
	DeviceBuffer device_buffer;
	MemoryPropertyFlags properties = 
		MemoryPropertyFlagBits::eDeviceLocal 
		| MemoryPropertyFlagBits::eHostVisible 
		| MemoryPropertyFlagBits::eHostCoherent;

	try {
		device_buffer = create_device_buffer(
			context,
			BufferUsageFlagBits::eUniformBuffer,
			properties,
			buffer_size
		);
	} catch (const std::runtime_error& e) {
		properties = MemoryPropertyFlagBits::eHostVisible | MemoryPropertyFlagBits::eHostCoherent;
		device_buffer = create_device_buffer(
			context,
			BufferUsageFlagBits::eUniformBuffer,
			properties,
			buffer_size
		);
	}

	return device_buffer;
}

void copy_buffer(const VulkanContext& context, Buffer src, Buffer dst, DeviceSize size) 
{
	// TODO: create copy function with fences
	BufferCopy buffer_copy (0, 0, size);
	CommandBuffer command_buffer = begin_single_use_command_buffer(context);
		command_buffer.copyBuffer(src, dst, 1, &buffer_copy);
	end_and_submit(context, command_buffer);
}

DeviceBuffer create_vertex_buffer(const VulkanContext& context, size_t buffer_size, void* vertex_data) 
{
	DeviceBuffer staging_buffer = create_device_buffer(
		context,
		BufferUsageFlagBits::eTransferSrc,
		MemoryPropertyFlagBits::eHostVisible | MemoryPropertyFlagBits::eHostCoherent,
		buffer_size
	);

	DeviceBuffer vertex_buffer = create_device_buffer(
		context,
		BufferUsageFlagBits::eTransferDst | BufferUsageFlagBits::eVertexBuffer,
		MemoryPropertyFlagBits::eDeviceLocal,
		buffer_size
	);

	copy_to_device(context.device, staging_buffer.device_memory, buffer_size, vertex_data);
	copy_buffer(context, staging_buffer.buffer, vertex_buffer.buffer, buffer_size);
	staging_buffer.free(context.device);

	return vertex_buffer;
}

DeviceBuffer create_index_buffer(const VulkanContext& context, size_t buffer_size, void* index_data) 
{
	DeviceBuffer staging_buffer = create_device_buffer(
		context,
		BufferUsageFlagBits::eTransferSrc,
		MemoryPropertyFlagBits::eHostVisible | MemoryPropertyFlagBits::eHostCoherent,
		buffer_size
	);

	DeviceBuffer index_buffer = create_device_buffer(
		context,
		BufferUsageFlagBits::eTransferDst | BufferUsageFlagBits::eIndexBuffer,
		MemoryPropertyFlagBits::eDeviceLocal,
		buffer_size
	);

	copy_to_device(context.device, staging_buffer.device_memory, buffer_size, index_data);
	copy_buffer(context, staging_buffer.buffer, index_buffer.buffer, buffer_size);
	staging_buffer.free(context.device);

	return index_buffer;
}

// images

Format find_format(PhysicalDevice physical_device, 
                   const vector<Format>& formats, 
                   ImageTiling tiling, 
                   FormatFeatureFlags features) 
{
	for (auto format : formats) {
		FormatProperties format_props = physical_device.getFormatProperties(format);
		if (tiling == ImageTiling::eLinear && (format_props.linearTilingFeatures & features) == features) {
			return format;
		}
		if (tiling == ImageTiling::eOptimal && (format_props.optimalTilingFeatures & features) == features) {
			return format;
		}
	}

	throw std::runtime_error("Couldn't find a depth format!");
}

Image create_image(Device device, 
                   Format image_format, 
                   Extent2D extent, 
		   uint32_t mip_levels,
                   ImageTiling tiling, 
                   ImageUsageFlags image_usage, 
                   uint32_t queue_family) 
{
	if (!Renderer::settings.use_mipmapping)
		mip_levels = 1;
	return device.createImage(
		{ {}, //flags
		  ImageType::e2D, // imageType
		  image_format, // format
		  Extent3D(extent.width, extent.height, 1), // extent
		  mip_levels, // mipLevels
		  1, // arrayLayers
		  SampleCountFlagBits::e1, // samples
		  tiling, // tiling
		  image_usage, // usage
		  SharingMode::eExclusive, // sharingMode 
		  1, // queueFamilyIndexCount 
		  &queue_family, // pQueueFamilyIndices
		  ImageLayout::eUndefined // initialLayout
		}
	);
}

DeviceMemory allocate_image_memory(const VulkanContext& context,
                                   Image image,
                                   MemoryPropertyFlags props) 
{
	MemoryRequirements m = context.device.getImageMemoryRequirements(image);
	uint32_t memory_type_index = find_memory_type(context.physical_device, props, m.memoryTypeBits);
	DeviceMemory mem = context.device.allocateMemory({m.size, memory_type_index});
	context.device.bindImageMemory(image, mem, 0);
	return mem;
}

ImageView create_image_view(Device device,
                            Image image,
                            Format format,
                            ImageAspectFlags aspect,
			    uint32_t mip_levels) 
{
	if (!Renderer::settings.use_mipmapping)
		mip_levels = 1;
	return device.createImageView(
		{ {}, //flags
		  image, // image
		  ImageViewType::e2D, // viewType
		  format, // format
		  { ComponentSwizzle::eIdentity, // components, r
		    ComponentSwizzle::eIdentity, // g
		    ComponentSwizzle::eIdentity, // b
		    ComponentSwizzle::eIdentity // a
		  },
		  {aspect, 0, mip_levels, 0, 1} // aspectMask, mipLevel, levelCount, baseArrayLayer, layerCount
		}
	);
}

SamplerCreateInfo get_default_sampler_create_info() 
{
	SamplerCreateInfo ci;
	ci.magFilter = Filter::eLinear; // magnification filter
	ci.minFilter = Filter::eLinear; // minification filter
	ci.mipmapMode = SamplerMipmapMode::eLinear; // mipmap mode
	// repeat, mirrored repeat, clamp to edge, clamp to border, mirror clamp to edge
	ci.addressModeU = SamplerAddressMode::eClampToEdge; 
	ci.addressModeV = SamplerAddressMode::eClampToEdge;
	ci.addressModeW = SamplerAddressMode::eClampToEdge;
	ci.mipLodBias = 0.0f; 
	//ci.anisotropyEnable = VK_TRUE;
	//ci.maxAnisotropy = 16;
	ci.anisotropyEnable = (VkBool32) Renderer::settings.use_anisotropy;
	ci.maxAnisotropy = Renderer::settings.max_anisotropy;
	ci.compareEnable = VK_FALSE;
	ci.compareOp = CompareOp::eAlways;
	ci.minLod = 0.0f;
	ci.maxLod = 0.0f;
	ci.borderColor = BorderColor::eIntOpaqueBlack;
	ci.unnormalizedCoordinates = VK_FALSE; // false, uv -> [0, 1], true uv -> [0, extent]
	return ci;
}

void copy_buffer_to_image(const VulkanContext& context, Buffer src, Image dst, Extent2D extent) 
{
	BufferImageCopy cpy (
		0,
		0,
		0,
		{ImageAspectFlagBits::eColor, 0, 0, 1},
		Offset3D(0, 0, 0),
		Extent3D(extent.width, extent.height, 1)
	);

	CommandBuffer command_buffer = begin_single_use_command_buffer(context);
		command_buffer.copyBufferToImage(src, dst, ImageLayout::eTransferDstOptimal, {cpy});
	end_and_submit(context, command_buffer);
}

bool linear_filtering_support(PhysicalDevice physical_device, Format format)
{
	FormatProperties format_properties = physical_device.getFormatProperties(format);
	return ((format_properties.optimalTilingFeatures & FormatFeatureFlagBits::eSampledImageFilterLinear) == FormatFeatureFlagBits::eSampledImageFilterLinear);
}

void generate_mipmaps(const VulkanContext& context, Format format, Image image, Extent2D extent, uint32_t mip_levels)
{
	if (!linear_filtering_support(context.physical_device, format))
		throw std::runtime_error("Linear formatting not supported for requested format");

	CommandBuffer cb = begin_single_use_command_buffer(context);

	ImageMemoryBarrier barrier;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
	barrier.subresourceRange = {ImageAspectFlagBits::eColor, 0, 1, 0, 1}; // mip level will change

	int32_t mip_width = extent.width;
	int32_t mip_height = extent.height;

	for (uint32_t i = 1; i < mip_levels; i++) {
		barrier.srcAccessMask = AccessFlagBits::eTransferWrite;
		barrier.dstAccessMask = AccessFlagBits::eTransferRead;
		barrier.oldLayout = ImageLayout::eTransferDstOptimal;
		barrier.newLayout = ImageLayout::eTransferSrcOptimal;
		barrier.subresourceRange.baseMipLevel = i - 1;
		cb.pipelineBarrier(
			PipelineStageFlagBits::eTransfer, PipelineStageFlagBits::eTransfer, DependencyFlags(),
			vector<MemoryBarrier>(),
			vector<BufferMemoryBarrier>(),
			{barrier}
		);

		ImageBlit blit (
			{ ImageAspectFlagBits::eColor,
			  i - 1,
			  0,
			  1
			},
			{{ Offset3D(0, 0, 0), 
			   Offset3D(mip_width, mip_height, 1)
			}},
			{ ImageAspectFlagBits::eColor,
			  i,
			  0,
			  1
			},
			{{ Offset3D(0, 0, 0), 
			   Offset3D(mip_width > 1 ? mip_width / 2 : 1, mip_height > 1 ? mip_height / 2 : 1, 1) // TODO this is sin
			}}
		);
		cb.blitImage(
			image, ImageLayout::eTransferSrcOptimal,
			image, ImageLayout::eTransferDstOptimal,
			{blit},
			Filter::eLinear
		);

		barrier.oldLayout = ImageLayout::eTransferSrcOptimal;
		barrier.newLayout = ImageLayout::eShaderReadOnlyOptimal;
		barrier.srcAccessMask = AccessFlagBits::eTransferRead;
		barrier.dstAccessMask = AccessFlagBits::eShaderRead;

		cb.pipelineBarrier(
			PipelineStageFlagBits::eTransfer, PipelineStageFlagBits::eFragmentShader, DependencyFlags(),
			vector<MemoryBarrier>(),
			vector<BufferMemoryBarrier>(),
			{barrier}
		);
		
		if (mip_width > 1) 
			mip_width /= 2;
		if (mip_height > 1) 
			mip_height /= 2;
	}

	barrier.srcAccessMask = AccessFlagBits::eTransferWrite;
	barrier.dstAccessMask = AccessFlagBits::eShaderRead;
	barrier.oldLayout = ImageLayout::eTransferDstOptimal;
	barrier.newLayout = ImageLayout::eShaderReadOnlyOptimal;
	barrier.subresourceRange.baseMipLevel = mip_levels - 1;
	cb.pipelineBarrier(
		PipelineStageFlagBits::eTransfer, PipelineStageFlagBits::eFragmentShader, DependencyFlags(),
		vector<MemoryBarrier>(),
		vector<BufferMemoryBarrier>(),
		{barrier}
	);

	end_and_submit(context, cb);
}

Texture create_texture(const VulkanContext& context, TextureCreateInfo ci) 
{
	bool use_mipmapping = Renderer::settings.use_mipmapping;
	if (!linear_filtering_support(context.physical_device, ci.image_format)) {
		log_info("mipmapping not supported");
		ci.mip_levels = 1;
		use_mipmapping = false;
	}

	Texture t;
	DeviceBuffer staging_buffer = create_device_buffer(
		context,
		BufferUsageFlagBits::eTransferSrc,
		MemoryPropertyFlagBits::eHostVisible | MemoryPropertyFlagBits::eHostCoherent,
		ci.buffer_size
	);
	copy_to_device(context.device, staging_buffer.device_memory, ci.buffer_size, ci.image_data);
	t.image = create_image(
		context.device, 
		ci.image_format, 
		ci.extent, 
		ci.mip_levels,
		ImageTiling::eOptimal, 
		ImageUsageFlagBits::eTransferDst | ImageUsageFlagBits::eTransferSrc | ImageUsageFlagBits::eSampled, 
		context.queue_families.graphics
	);
	t.memory = allocate_image_memory(context, t.image);
	transition_image_layout(
		context, 
		t.image, 
		ci.image_format, 
		ImageLayout::eUndefined, 
		ImageLayout::eTransferDstOptimal,
		ci.mip_levels
	);
	copy_buffer_to_image(context, staging_buffer.buffer, t.image, ci.extent);
	staging_buffer.free(context.device);
	if (use_mipmapping) {
		generate_mipmaps(context, ci.image_format, t.image, ci.extent, ci.mip_levels);
	} else {
		transition_image_layout(
			context, 
			t.image, 
			ci.image_format, 
			ImageLayout::eTransferDstOptimal, 
			ImageLayout::eShaderReadOnlyOptimal,
			1
		);
	}
	t.image_view = create_image_view(context.device, t.image, ci.image_format, ImageAspectFlagBits::eColor, ci.mip_levels);
	t.sampler = ci.sampler;

	return t;
}

Texture create_device_local_texture(const VulkanContext& context, TextureCreateInfo ci, ImageUsageFlags image_usage, ImageLayout desired_layout)
{
	// mipmapping ignored
	Texture t;

	t.image = create_image(
		context.device, 
		ci.image_format, 
		ci.extent, 
		1,
		ImageTiling::eOptimal, 
		image_usage, 
		context.queue_families.graphics
	);
	t.memory = allocate_image_memory(context, t.image);

	if (desired_layout != ImageLayout::eUndefined) {
		transition_image_layout(
			context, 
			t.image, 
			ci.image_format, 
			ImageLayout::eUndefined, 
			desired_layout,
			1
		);
	}

	t.image_view = create_image_view(context.device, t.image, ci.image_format, ImageAspectFlagBits::eColor, 1);
	t.sampler = ci.sampler;

	return t;
}

void transition_image_layout(const VulkanContext& context, 
                             Image image, 
                             Format format, 
                             ImageLayout old_layout, 
                             ImageLayout new_layout,
			     uint32_t mip_levels) 
{
	CommandBuffer command_buffer = begin_single_use_command_buffer(context);
	PipelineStageFlags src_stage;
	PipelineStageFlags dst_stage;
	ImageMemoryBarrier barrier;
	if (old_layout == ImageLayout::eUndefined) {
		barrier = ImageMemoryBarrier (
			AccessFlags(),
			AccessFlagBits::eTransferWrite,
			old_layout,
			new_layout,
			VK_QUEUE_FAMILY_IGNORED,
			VK_QUEUE_FAMILY_IGNORED,
			image,
			{ImageAspectFlagBits::eColor, 0, mip_levels, 0, 1}
		);
		src_stage = PipelineStageFlagBits::eTopOfPipe;
		dst_stage = PipelineStageFlagBits::eTransfer;
	} else {
		barrier = ImageMemoryBarrier(
			AccessFlagBits::eTransferWrite,
			AccessFlagBits::eShaderRead,
			old_layout,
			new_layout,
			VK_QUEUE_FAMILY_IGNORED,
			VK_QUEUE_FAMILY_IGNORED,
			image,
			{ImageAspectFlagBits::eColor, 0, mip_levels, 0, 1}
		);
		src_stage = PipelineStageFlagBits::eTransfer;
		dst_stage = PipelineStageFlagBits::eFragmentShader;
	}

	command_buffer.pipelineBarrier(src_stage, dst_stage, DependencyFlags(), 0, nullptr, 0, nullptr, 1, &barrier);
	end_and_submit(context, command_buffer);
}

uint32_t get_mip_levels(uint32_t res)
{
	return static_cast<uint32_t>(std::floor(std::log2(res))) + 1;
}

CommandBuffer begin_single_use_command_buffer(const VulkanContext& context) 
{
	CommandBuffer command_buffer = context.device.allocateCommandBuffers(
		{ context.command_pool,
		  CommandBufferLevel::ePrimary,
		  1
		}
	)[0];
	command_buffer.begin({CommandBufferUsageFlagBits::eOneTimeSubmit});
	return command_buffer;
}

void end_and_submit(const VulkanContext& context, vk::CommandBuffer command_buffer) 
{
	command_buffer.end();
	SubmitInfo submit_info; 
	submit_info.commandBufferCount = 1;
	submit_info.pCommandBuffers = &command_buffer;
	context.graphics_queue.submit(1, &submit_info, Fence());
	context.graphics_queue.waitIdle();
	context.device.freeCommandBuffers(context.command_pool, 1, &command_buffer);
}

// Renderer Class
void Renderer::init() 
{
	load_settings();

	// TODO: Error checking
	window = SDL_CreateWindow(
		appdata.name,
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		appdata.resolution[0], appdata.resolution[1],
		window_flags
	);

	{ // create instance
		unsigned int instance_extension_count;
		SDL_Vulkan_GetInstanceExtensions(window, &instance_extension_count, nullptr);

		vector<const char*> instance_extensions;
		instance_extensions.resize(instance_extension_count);

		SDL_Vulkan_GetInstanceExtensions(window, &instance_extension_count, instance_extensions.data());

		log_info("Instance extensions:", instance_extensions);

		vector<const char*> validation_layers = {
			//"VK_LAYER_LUNARG_standard_validation"
		};
		
		uint32_t api_version;
		enumerateInstanceVersion(&api_version);

		ApplicationInfo ai (
			appdata.name,
			VK_MAKE_VERSION(0, 0, 1),
			"engine",
			VK_MAKE_VERSION(0, 0, 1),
			api_version
		);
		InstanceCreateInfo ci (
			InstanceCreateFlags(),
			&ai,
			validation_layers.size(),
			validation_layers.data(),
			instance_extension_count,
			instance_extensions.data()
		);

		instance = createInstance(ci);
	}

	// acquire surface
	SDL_Vulkan_CreateSurface(window, instance, (VkSurfaceKHR*) &surface);

	{ // pick physical device
		vector<PhysicalDevice> physical_devices;
		try {
			physical_devices = instance.enumeratePhysicalDevices();
		} catch (const InitializationFailedError& e) {
			log_error("No supported devices found. Make sure your graphics driver supports Vulkan.");
			throw e;
		}

		vector<std::string> device_names;
		int i = 0;
		for (auto d : physical_devices) {
			auto p = d.getProperties();
			if (i == settings.device_index)
				min_uniform_buffer_alignment = p.limits.minUniformBufferOffsetAlignment;
			device_names.push_back(p.deviceName);
			i++;
		}
		log_info("Physical Devices:");
		for (auto& device_name : device_names)
			log_info("    %s", device_name.c_str());

		int device_index = settings.device_index;
		if (device_index < 0 || device_index > physical_devices.size() - 1) {
			char buffer[256];
			sprintf(
				buffer, 
				"Requested device index %d must be positive and less than the number of devices found (%d)", 
				device_index, physical_devices.size()
			);
			throw std::runtime_error(buffer);
		}

		physical_device = physical_devices[device_index];
	}

	{
		SurfaceCapabilitiesKHR capabilities = physical_device.getSurfaceCapabilitiesKHR(surface);

		uint32_t image_count = capabilities.minImageCount + 1;
		if (image_count > capabilities.maxImageCount && capabilities.maxImageCount > 0)
			image_count = capabilities.maxImageCount;

		log_info("Swapchain image count: %d", image_count);
	}

	{ // queue families
		vector<QueueFamilyProperties> queue_family_properties = physical_device.getQueueFamilyProperties();
		int i = 0;
		for (auto qfp : queue_family_properties) {
			if (qfp.queueFlags & QueueFlagBits::eGraphics) {
				queue_families.graphics = i;
				bool present_support = physical_device.getSurfaceSupportKHR(i, surface);
				if (present_support)
					queue_families.present = i;
			}
			if (queue_families.graphics >= 0 && queue_families.present >= 0)
				break;
			i++;
		}
	}

	{ // create logical device
		// TODO transfer & Present queue?
		uint32_t queue_count;
		const float priorities[1] = {1.f};
		vector<DeviceQueueCreateInfo> queue_create_infos = {
			DeviceQueueCreateInfo({}, queue_families.graphics, 1, priorities)
		};
		queue_count = queue_create_infos.size();
		uint32_t device_layer_count;
		vector<const char*> device_layers = {
			//"VK_LAYER_LUNARG_standard_validation"
		};
		device_layer_count = device_layers.size();
		uint32_t device_extension_count;
		vector<const char*> device_extensions = {
			"VK_KHR_swapchain",
			//"VK_GOOGLE_display_timing"
		};

		PhysicalDeviceFeatures device_features;
		device_features.samplerAnisotropy = VK_TRUE;
		auto extension_properties = physical_device.enumerateDeviceExtensionProperties();
		for (auto ep : extension_properties) {
			if (std::string("VK_GOOGLE_display_timing").compare(ep.extensionName) == 0){
				enable_display_timing = settings.use_display_timing;
				if (enable_display_timing) {
					log_info("Using extension VK_GOOGLE_display_timing");
					device_extensions.push_back("VK_GOOGLE_display_timing");
				}
			}
		}

		log_info("Device extensions:", device_extensions);
		device_extension_count = device_extensions.size();

		device = physical_device.createDevice(
			{ DeviceCreateFlags(),
			  queue_count,
			  queue_create_infos.data(),
			  device_layer_count,
			  device_layers.data(),
			  device_extension_count,
			  device_extensions.data(),
			  &device_features
			}
		);

		graphics_queue = device.getQueue(queue_families.graphics, 0);
		present_queue = graphics_queue;
	}

	dl = DispatchLoaderDynamic();
	dl.init<>(instance, device);
	//dl = DispatchLoaderDynamic<>(instance, device);
	command_pool = device.createCommandPool({CommandPoolCreateFlagBits::eTransient, queue_families.graphics});

	context = {
		instance,
		surface,
		physical_device,
		device,
		dl,
		queue_families,
		graphics_queue,
		command_pool,
	};

	init_pass();

	{ // create semaphores
		image_available = device.createSemaphore(SemaphoreCreateInfo());
		render_finished = device.createSemaphore(SemaphoreCreateInfo());
	}

	copy_shader = new PostProcessRenderer(context, &_binary_copy_frag_glsl_spv_start, &_binary_copy_frag_glsl_spv_end);
	if (settings.use_fxaa)
		fxaa_renderer = new PostProcessRenderer(context, &_binary_fxaa_frag_glsl_spv_start, &_binary_fxaa_frag_glsl_spv_end);
}

void Renderer::init_pass(vk::SwapchainKHR old_swapchain) 
{
	{ // create swapchain
		// TODO: query surface format & swap method
		swapchain_extent = Extent2D (0, 0);
		SDL_Vulkan_GetDrawableSize(window, (int*) &swapchain_extent.width, (int*) &swapchain_extent.height);
		surface_format = Format::eB8G8R8A8Srgb;
		PresentModeKHR present_mode = PresentModeKHR::eFifo;

		// """error checking""" >;3€
		vector<SurfaceFormatKHR> supported_formats = physical_device.getSurfaceFormatsKHR(surface);
		bool found = false;
		for (auto format : supported_formats)
			if (surface_format == format.format) {
				found = true;
				break;
			}
		if (!found) {
			throw runtime_error(
				"Standard surface format isn't supported, try again on something that isn't a potato..."
			);
		}
		vector<PresentModeKHR> supported_modes = physical_device.getSurfacePresentModesKHR(surface); 
		found = false;
		for (auto mode : supported_modes)
			if (mode == present_mode) {
				found = true;
				break;
			}
		if (!found) {
			present_mode = PresentModeKHR::eFifo; // guaranteed to be supported.
		}
		// to appease the validation gods
		SurfaceCapabilitiesKHR capabilities = physical_device.getSurfaceCapabilitiesKHR(surface);
		int sdl_w, sdl_h;
		SDL_GetWindowSize(window, &sdl_w, &sdl_h);
		swapchain_extent = Extent2D(
			max(capabilities.minImageExtent.width, min(uint32_t(sdl_w), capabilities.maxImageExtent.width)), 
			max(capabilities.minImageExtent.height, min(uint32_t(sdl_h), capabilities.maxImageExtent.height))
		);

		uint32_t image_count = capabilities.minImageCount + 1;
		if (image_count > capabilities.maxImageCount && capabilities.maxImageCount > 0)
			image_count = capabilities.maxImageCount;

		swapchain = device.createSwapchainKHR(
			{ {}, // flags
			  surface,
			  image_count,
			  surface_format, // surface format
			  ColorSpaceKHR::eSrgbNonlinear, // color space
			  swapchain_extent,
			  1, // image array layers
			  ImageUsageFlagBits::eColorAttachment, // usage.
			  SharingMode::eExclusive, // TODO: make concurrent
			  1, // queue family index count
			  &queue_families.present,
			  SurfaceTransformFlagBitsKHR::eIdentity, // surface transform
			  CompositeAlphaFlagBitsKHR::eOpaque, // MAKE TRANSPARENT BECAUSE IT'S COOL DAMN IT
			  present_mode,
			  false, // clipped? investigate
			  old_swapchain,
			}
		);

		swapchain_images = device.getSwapchainImagesKHR(swapchain);

		context.swapchain_image_extent = swapchain_extent;
		context.surface_format = surface_format;
		context.images = swapchain_images.size();
	}

	{ // create first pass color attachments
		color_attachments.clear();
		color_attachments.resize(context.images);
		SamplerCreateInfo attachment_sampler_ci = get_default_sampler_create_info();
		//attachment_sampler_ci.unnormalizedCoordinates = VK_TRUE;
		//attachment_sampler_ci.mipmapMode = SamplerMipmapMode::eNearest;
		//attachment_sampler_ci.addressModeU = SamplerAddressMode::eClampToEdge;
		//attachment_sampler_ci.addressModeV = SamplerAddressMode::eClampToEdge;
		//attachment_sampler_ci.addressModeW = SamplerAddressMode::eClampToEdge;
		//attachment_sampler_ci.anisotropyEnable = VK_FALSE;
		for (auto& att : color_attachments) {
			Sampler attachment_sampler = device.createSampler(attachment_sampler_ci);
			TextureCreateInfo ci (
				0, // ignored
				nullptr, // ignored
				surface_format,
				swapchain_extent,
				1,
				attachment_sampler
			);
			att = create_device_local_texture(
				context, ci, 
				ImageUsageFlagBits::eColorAttachment | ImageUsageFlagBits::eSampled,
				ImageLayout::eUndefined
			);
		}
	}

	Format depth_format; 
	{ // create depth image
		vector<Format> depth_formats = {Format::eD24UnormS8Uint, Format::eD32SfloatS8Uint, Format::eD32Sfloat};
		depth_format = find_format(
			physical_device, 
			depth_formats, 
			ImageTiling::eOptimal, 
			FormatFeatureFlagBits::eDepthStencilAttachment
		);
		depth_image = create_image(
			device, 
			depth_format, 
			swapchain_extent, 
			1,
			ImageTiling::eOptimal, 
			ImageUsageFlagBits::eDepthStencilAttachment, 
			queue_families.graphics
		);
		depth_image_memory = allocate_image_memory(context, depth_image);
		depth_image_view = create_image_view(device, depth_image, depth_format, ImageAspectFlagBits::eDepth, 1);
	} // end create depth image

	{ // create render pass

		std::array<AttachmentDescription, 2> attachments{};
		// Color attachment descripton
		AttachmentDescription cad (
			{}, // flags
			surface_format,
			SampleCountFlagBits::e1,
			AttachmentLoadOp::eClear, // attachment load
			AttachmentStoreOp::eStore,// attachment store
			AttachmentLoadOp::eDontCare, // stencil load
			AttachmentStoreOp::eDontCare, // stencil store
			ImageLayout::eUndefined, // initial layout
			ImageLayout::eShaderReadOnlyOptimal // final layout
		);

		attachments[0] = cad;

		// depth attachment description
		AttachmentDescription depth_ad (
			{}, // flags
			depth_format,
			SampleCountFlagBits::e1,
			AttachmentLoadOp::eClear, // attachment load
			AttachmentStoreOp::eDontCare,// attachment store
			AttachmentLoadOp::eDontCare, // stencil load
			AttachmentStoreOp::eDontCare, // stencil store
			ImageLayout::eUndefined, // initial layout
			ImageLayout::eDepthStencilAttachmentOptimal // final layout
		);

		attachments[1] = depth_ad;

		AttachmentReference car (
			0,
			ImageLayout::eColorAttachmentOptimal
		);

		AttachmentReference depth_ar (
			1,
			ImageLayout::eDepthStencilAttachmentOptimal
		);


		SubpassDescription color_pass (
			{},
			PipelineBindPoint::eGraphics,
			0,
			nullptr,
			1,
			&car,
			nullptr,
			&depth_ar,
			0,
			nullptr
		);

		render_pass = device.createRenderPass(
			{ {}, // flags
			  attachments.size(), // attachment count
			  attachments.data(),
			  1,
			  &color_pass,
			  0,
			  nullptr,
			}
		);
		context.render_pass = render_pass;

		// render pass 2 for post processing

		// Swapchain attachment description
		AttachmentDescription sad (
			{}, // flags
			surface_format,
			SampleCountFlagBits::e1,
			AttachmentLoadOp::eClear, // attachment load
			AttachmentStoreOp::eStore,// attachment store
			AttachmentLoadOp::eDontCare, // stencil load
			AttachmentStoreOp::eDontCare, // stencil store
			ImageLayout::eUndefined, // initial layout
			ImageLayout::ePresentSrcKHR // final layout
		);

		AttachmentReference sar (
			0,
			ImageLayout::eColorAttachmentOptimal
		);

		SubpassDescription post_pass (
			{},
			PipelineBindPoint::eGraphics,
			0,
			nullptr,
			1,
			&sar,
			nullptr,
			nullptr,
			0,
			nullptr
		);

		post_process_render_pass = device.createRenderPass(
			{ {}, // flags
			  1, // attachment count
			  &sad,
			  1,
			  &post_pass,
			  0,
			  nullptr,
			}
		);
		context.post_process_render_pass = post_process_render_pass;
	}

	{ // create framebuffers
		swapchain_image_views.clear();
		swapchain_image_views.resize(swapchain_images.size());
		for (int i = 0; i < swapchain_images.size(); i++) 
			swapchain_image_views[i] = device.createImageView(
				{ {}, //flags
				  swapchain_images[i],
				  ImageViewType::e2D,
				  surface_format,
				  { ComponentSwizzle::eIdentity,
				    ComponentSwizzle::eIdentity,
				    ComponentSwizzle::eIdentity,
				    ComponentSwizzle::eIdentity
				  },
				  { ImageAspectFlagBits::eColor, 0, 1, 0, 1}
				}
			);

		framebuffers.clear();
		framebuffers.resize(swapchain_images.size());
		for (int i = 0; i < swapchain_image_views.size(); i++) {
			ImageView views[2] = {color_attachments[i].image_view, depth_image_view};
			framebuffers[i] = device.createFramebuffer(
				{ {}, // flags
				  render_pass,
				  2,
				  views,
				  swapchain_extent.width,
				  swapchain_extent.height,
				  1
				}
			);
		}

		post_process_framebuffers.clear();
		post_process_framebuffers.resize(swapchain_images.size());
		for (int i = 0; i < swapchain_image_views.size(); i++) {
			ImageView views[1] = {swapchain_image_views[i]};
			post_process_framebuffers[i] = device.createFramebuffer(
				{ {}, // flags
				  post_process_render_pass,
				  1,
				  views,
				  swapchain_extent.width,
				  swapchain_extent.height,
				  1
				}
			);
		}

	} // end create framebuffers


	create_pipelines();
	create_descriptors();
	record_render_pass_command_buffers();
}

void Renderer::create_descriptors() 
{
	for (int i = 0; i < descriptors_to_create.size(); i++) {
		Descriptor* d = descriptors_to_create[i];
		d->create_descriptors(context);
		created_descriptors.push_back(d);
	}
	descriptors_to_create.clear();

	for (int i = 0; i < pp_descriptors_to_create.size(); i++) {
		PostProcessDescriptor* d = pp_descriptors_to_create[i];
		d->create_descriptors(context, color_attachments);
		created_pp_descriptors.push_back(d);
	}
	pp_descriptors_to_create.clear();
}

void Renderer::destroy_descriptors() 
{
	for (int i = 0; i < created_pp_descriptors.size(); i++) {
		auto p = created_pp_descriptors[i];
		p->destroy_descriptors(context);
	}
	pp_descriptors_to_create = created_pp_descriptors;
	created_pp_descriptors.clear();
	created_pp_descriptors.reserve(pp_descriptors_to_create.size());
}

void Renderer::create_pipelines() 
{
	for (int i = 0; i < pipelines_to_create.size(); i++) {
		auto p = pipelines_to_create[i];
		p->create_pipeline(context);
		created_pipelines.push_back(p);
	}
	pipelines_to_create.clear();
}

void Renderer::destroy_pipelines() 
{
	for (int i = 0; i < created_pipelines.size(); i++) {
		auto p = created_pipelines[i];
		p->destroy_pipeline(context);
	}
	pipelines_to_create = created_pipelines;
	created_pipelines.clear();
	created_pipelines.reserve(pipelines_to_create.size());
}

void Renderer::record_render_pass_command_buffers() 
{
	auto record_command_buffer_vec = [this](auto& cbvec, auto& rcbvec, RenderPass rp, vector<Framebuffer> fbs)
	{
		for (int i = 0; i < rcbvec.size(); i++) {
			auto cb = rcbvec[i];
			cb->allocate_command_buffers(context);
			for (int j = 0; j < framebuffers.size(); j++) {
				CommandBufferInheritanceInfo inheritance_info (
					rp,
					0,
					fbs[j],
					VK_FALSE,
					{},
					{}
				);
				cb->record_command_buffer(context, inheritance_info, j);
			}
			cbvec.push_back(cb);
		}
		rcbvec.clear();
	};
	record_command_buffer_vec(recorded_command_buffers, command_buffers_to_record, render_pass, framebuffers);
	record_command_buffer_vec(recorded_pp_command_buffers, pp_command_buffers_to_record, post_process_render_pass, post_process_framebuffers);

	command_buffers.resize(framebuffers.size());
	CommandBufferAllocateInfo ai (command_pool, CommandBufferLevel::ePrimary, (uint32_t) command_buffers.size());
	device.allocateCommandBuffers(&ai, command_buffers.data());

	int i = 0;
	for (auto cb : command_buffers) {
		cb.begin({CommandBufferUsageFlagBits::eSimultaneousUse});
		
		ClearColorValue cv (array<float, 4> {0.05f, 0.05f, 0.05f, 1.0f});
		ClearValue c (cv);
		ClearValue dc = ClearValue();
		dc.depthStencil = ClearDepthStencilValue(1.0f, 0);
		ClearValue clears[2] = {c, dc};
		RenderPassBeginInfo pass_info (
			render_pass,
			framebuffers[i],
			Rect2D({0,0}, swapchain_extent),
			2,
			clears
		);

		cb.beginRenderPass(&pass_info, SubpassContents::eSecondaryCommandBuffers);
			for (auto subbuffer : recorded_command_buffers) {
				cb.executeCommands(subbuffer->get_command_buffers()[i]);
			}
		cb.endRenderPass();

		RenderPassBeginInfo pp_pass (
			post_process_render_pass,
			post_process_framebuffers[i],
			Rect2D({0,0}, swapchain_extent),
			1,
			clears
		);
		cb.beginRenderPass(&pp_pass, SubpassContents::eSecondaryCommandBuffers);
			for (auto subbuffer : recorded_pp_command_buffers) {
				cb.executeCommands(subbuffer->get_command_buffers()[i]);
			}
		cb.endRenderPass();

		cb.end();
		i++;
	}
}

void Renderer::free_render_pass_command_buffers() 
{
	auto clear_command_buffer_vectors = [this](auto& cbvec, auto& rcbvec)
	{
		for (auto cb : cbvec) {
			cb->free_command_buffers(context);
			rcbvec.push_back(cb);
		}
		cbvec.clear();
		cbvec.reserve(rcbvec.size());
	};
	clear_command_buffer_vectors(recorded_command_buffers, command_buffers_to_record);
	clear_command_buffer_vectors(recorded_pp_command_buffers, pp_command_buffers_to_record);

	device.freeCommandBuffers(command_pool, command_buffers);
}

uint32_t Renderer::draw_frame(uint64_t scheduled_present_time, World& w) 
{
	if (assets_changed) {
		create_new_assets();
		assets_changed = false;
	}
	uint32_t image_index;
	try {
		auto result = device.acquireNextImageKHR(swapchain, numeric_limits<uint64_t>::max(), image_available, nullptr);
		if (result.result == Result::eErrorOutOfDateKHR) {
			log_info("Acquire next image result == out of date.");
			resize(w);
			return present_id - 1;
		} 
		image_index = result.value;
	} catch (const OutOfDateKHRError& e) {
		log_info("Caught exception OutOfDateKHRError.");
		resize(w);
		return present_id - 1;
	}

	w.run_render_systems(context, image_index);
//	for (auto s : w.render_systems) {
//		s->run(w, context, image_index);
//	}

	PipelineStageFlags sf (PipelineStageFlagBits::eColorAttachmentOutput);
	SubmitInfo si (
		1,
		&image_available,
		&sf,
		1,
		&command_buffers[image_index],
		1,
		&render_finished
	);

	graphics_queue.submit(1, &si, nullptr);


	try {
		if (enable_display_timing) {
			PresentTimeGOOGLE pt (present_id, scheduled_present_time);
			//PresentTimeGOOGLE pt (present_id, 0);
			PresentTimesInfoGOOGLE pti (1, &pt);
			PresentInfoKHR pi (
				1,
				&render_finished,
				1,
				&swapchain,
				&image_index
			);
			present_id++;
			StructureChain<PresentInfoKHR, PresentTimesInfoGOOGLE> sc = {
				pi,
				pti
			};
			auto result = present_queue.presentKHR(sc.get<PresentInfoKHR>());
			if (result == Result::eErrorOutOfDateKHR || result == Result::eSuboptimalKHR) {
				log_info("Present result is out of date or suboptimal (display timing)");
				resize(w);
			}
		} else {
			PresentInfoKHR pi (
				1,
				&render_finished,
				1,
				&swapchain,
				&image_index
			);
			auto result = present_queue.presentKHR(pi);
			if (result == Result::eErrorOutOfDateKHR || result == Result::eSuboptimalKHR) {
				log_info("Present result is out of date or suboptimal");
				resize(w);
			}
		}
	} catch (const OutOfDateKHRError& e) {
		log_info("Caught exception out of date on present");
		resize(w);
	} catch (const SystemError& e) {
		resize(w);
	}
	return present_id - 1;
}

void Renderer::print_frame_info() 
{
	auto past_presentation_timing = device.getPastPresentationTimingGOOGLE(swapchain, dl);
	for (auto t : past_presentation_timing) {
		log_update(
			"Frame %d Difference: %li Margin %lu", 
			t.presentID, 
			int64_t(t.actualPresentTime - t.earliestPresentTime), 
			t.presentMargin
		);
	}
}

void Renderer::wait_idle() 
{
	device.waitIdle();
}

void Renderer::resize(World& w)
{
	if (assets_changed) {
		create_new_assets();
		assets_changed = false;
	}
	reset_swapchain();
	for (auto& c : w.get_components<CameraComponent>()) {
		c.update(swapchain_extent.width, swapchain_extent.height);
	}
}

void Renderer::load_settings()
{
#define LOAD_SETTING( setting, type ) settings.setting = get_var<type>(#setting, defaults.setting);
	RendererSettings defaults;
	LOAD_SETTING(use_display_timing, bool);
	LOAD_SETTING(use_anisotropy, bool);
	LOAD_SETTING(max_anisotropy, int);
	LOAD_SETTING(use_mipmapping, bool);
	LOAD_SETTING(use_fxaa, bool);
	LOAD_SETTING(device_index, int);
}

void Renderer::add_pipeline(RenderPipeline* p) 
{
	assets_changed = true;
	pipelines_to_create.push_back(p);
}

void Renderer::add_render_pass_command_buffer(RenderPassCommandBuffer* cb)
{
	assets_changed = true;
	command_buffers_to_record.push_back(cb);
}

void Renderer::add_post_process_command_buffer(PostProcessCommandBuffer* cb)
{
	assets_changed = true;
	pp_command_buffers_to_record.push_back(cb);
}

void Renderer::add_descriptor(Descriptor* d)
{
	assets_changed = true;
	descriptors_to_create.push_back(d);
}

void Renderer::add_post_process_descriptor(PostProcessDescriptor* d)
{
	assets_changed = true;
	pp_descriptors_to_create.push_back(d);
}

uint64_t Renderer::get_refresh_cycle_duration() 
{
	auto timing_properties = device.getRefreshCycleDurationGOOGLE(swapchain, dl);
	return timing_properties.refreshDuration;
}

uint64_t Renderer::get_next_present_time(uint64_t desired_delta) 
{
	auto past_presentation_timing = device.getPastPresentationTimingGOOGLE(swapchain, dl);
	for (auto t : past_presentation_timing) {
		log_update(
			"Frame %d Difference: %li Margin %lu", 
			t.presentID, 
			int64_t(t.actualPresentTime - t.earliestPresentTime), 
			t.presentMargin
		);
	}
	return 0;
}

const VulkanContext& Renderer::get_context() 
{
	return context;
}

void Renderer::create_new_assets() 
{
	create_pipelines();
	create_descriptors();
	free_render_pass_command_buffers();
	record_render_pass_command_buffers();
}

void Renderer::destroy_pass() 
{
	destroy_descriptors();
	destroy_pipelines();
	free_render_pass_command_buffers();
	for (auto framebuffer : framebuffers)
		device.destroyFramebuffer(framebuffer);
	for (auto framebuffer : post_process_framebuffers)
		device.destroyFramebuffer(framebuffer);
	for (auto att : color_attachments)
		att.destroy(context.device);
	for (auto image_view : swapchain_image_views)
		device.destroyImageView(image_view);
	device.destroyImage(depth_image);
	device.destroyImageView(depth_image_view);
	device.freeMemory(depth_image_memory);
	device.destroyRenderPass(post_process_render_pass);
	device.destroyRenderPass(render_pass);
	device.destroySwapchainKHR(swapchain);
}

void Renderer::reset_swapchain() 
{
	log_info("Resetting swapchain");
	wait_idle();
	destroy_pass();
	init_pass();
}

Renderer::Renderer(ApplicationData d) 
		: appdata(d)
		, window_flags(
			//SDL_WINDOW_BORDERLESS | 
			SDL_WINDOW_RESIZABLE |
			SDL_WINDOW_VULKAN
		) {}

Renderer::~Renderer() 
{
	wait_idle();
	copy_shader->cleanup();
	if (settings.use_fxaa)
		fxaa_renderer->cleanup();
	device.destroySemaphore(image_available);
	device.destroySemaphore(render_finished);
	destroy_pass();
	delete copy_shader;
	if (settings.use_fxaa)
		delete fxaa_renderer;
	device.destroyCommandPool(command_pool);
	device.destroy();
	instance.destroySurfaceKHR(surface);
	instance.destroy();
	SDL_DestroyWindow(window);
}
}
