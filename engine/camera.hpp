#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace engine {
struct CameraComponent {
	float fov;
	glm::mat4 matrix = glm::mat4(0.0f);
	uint32_t entity_id;

	glm::mat4& get_proj() 
	{
		return matrix;
	}

	void update(float width, float height, float near = 0.1, float far = 100.0) 
	{
		matrix = glm::perspectiveFov<float>(fov, width, height, near, far);
		matrix[1][1] *= -1;
	}

	CameraComponent(float fov_, float width, float height, float near = 0.1, float far = 100.0) : fov(fov_) 
	{
		matrix = glm::perspectiveFov<float>(fov, width, height, near, far);
		matrix[1][1] *= -1;
	}
};
}
