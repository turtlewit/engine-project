#include <chrono>
#include <thread>
#include <stdexcept>
#include <csignal>
#include <array>
#include <functional>

#include <SDL2/SDL.h>
#include <SDL2/SDL_events.h>
#include <SDL2/SDL_image.h>

#include "engine.hpp"
#include "application.hpp"
#include "system.hpp"
#include "renderer.hpp"
#include "logger.hpp"
#include "gameshell.hpp"

using std::runtime_error,
      std::string,
      std::vector;

namespace engine {

void Input::register_action(const string& name, SDL_Keycode keycode) 
{
	bool found = false;
	uint32_t key;
	int i = 0;

	for (KeyboardKey& k : keys) {
		if (k.keycode == keycode) {
			found = true;
			key = i;
			break;
		}
		i++;
	}

	if (!found) {
		key = keys.size();
		keys.push_back({keycode});
		keycodes.push_back(keycode);
		keycode_map[keycode] = key;
	}

	if (keymap.find(name) == keymap.end()) {
		keymap[name] = vector<uint32_t>();
		keymap[name].push_back(key);
	} else {
		keymap[name].push_back(key);
	}
}

bool Input::get_action_down(const string& name) 
{
	int num = 0;

	for (auto k : keymap[name]) {
		num += keys[k].press;
	}

	return num > 0;
}

bool Input::get_action(const string& name) 
{
	int num = 0;

	for (auto k : keymap[name]) {
		num += keys[k].hold;
	}

	return num > 0;
}

bool Input::get_action_up(const string& name) 
{
	int num = 0;

	for (auto k : keymap[name]) {
		num += keys[k].release;
	}

	return num > 0;
}

void Input::set_key_down(SDL_Keycode keycode) 
{
	keys[keycode_map[keycode]].press = true;
	keys[keycode_map[keycode]].hold = true;
}

void Input::set_key_up(SDL_Keycode keycode) 
{
	keys[keycode_map[keycode]].hold = false;
	keys[keycode_map[keycode]].release = true;
}

const vector<SDL_Keycode> Input::get_keycodes() 
{
	return keycodes;
}

void Input::reset() 
{
	for (KeyboardKey& key : keys) {
		key.press = false;
		key.release = false;
	}
	mouse_rel_x = 0;
	mouse_rel_y = 0;
}

Application* Application::app = nullptr;

Input Application::input = Input();

Application* Application::get_app() 
{
	if (app == nullptr) 
		throw runtime_error("Application has not been initialized!");
	return app;
}

Renderer* Application::get_renderer() 
{
	auto app = get_app();

	if (app->renderer == nullptr)
		throw runtime_error("Renderer has not been initialized!");

	return app->renderer;
}

std::array<uint32_t, 2> Application::get_screen_size() 
{
	auto r = Application::get_renderer();
	const VulkanContext& context = r->get_context();
	return {{context.swapchain_image_extent.width, context.swapchain_image_extent.height}};
}

void Application::handle_events() 
{
	input.reset();
	SDL_Event e;
	const vector<SDL_Keycode> keycodes = input.get_keycodes();

	auto handle_keypress = [keycodes]( void (Input::*fn)(SDL_Keycode keycode), 
	                                   SDL_Event e ) 
	{
		for (SDL_Keycode keycode : keycodes) {
			if (e.key.keysym.sym == keycode) {
				(input.*fn)(keycode);
				break;
			}
		}

	};
	while (SDL_PollEvent(&e)) {
		switch (e.type) {
		case SDL_WINDOWEVENT:
			switch (e.window.event) {
			case SDL_WINDOWEVENT_CLOSE :
				running = false;
			case SDL_WINDOWEVENT_RESIZED:
				renderer->resize(current_world);
			}
			break;
		case SDL_KEYDOWN:
			handle_keypress(&Input::set_key_down, e);
			break;
		case SDL_KEYUP:
			handle_keypress(&Input::set_key_up, e);
			break;
		case SDL_MOUSEMOTION:
			input.mouse_rel_x += e.motion.xrel;
			input.mouse_rel_y += e.motion.yrel;
			break;
		case SDL_MOUSEBUTTONDOWN:
			input.mouse_click = true;
			break;
		case SDL_MOUSEBUTTONUP:
			input.mouse_click = false;
			break;
		}
	}
}

uint64_t get_time_ns() 
{
	return std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
}

float get_delta(uint64_t refresh, uint16_t mul) 
{
	return float(refresh) / float(1000000000) * mul;
}

uint64_t get_present_time(uint64_t start, uint64_t refresh, uint16_t multiple) 
{
	return uint64_t(start + (refresh * multiple));
}

float get_initial_delta() 
{
	int modes = SDL_GetNumDisplayModes(0);

	for (int i = 0; i < modes; i++) {
		SDL_DisplayMode mode;
		SDL_GetDisplayMode(0, i, &mode);
		if (mode.refresh_rate > 0)
			return 1.f / float(mode.refresh_rate);
	}

	return 1.0f / 60.0f;
}

class DeltaTimer {
	std::chrono::time_point<std::chrono::high_resolution_clock> start;
	std::chrono::nanoseconds last_delta;
	
	public:
	DeltaTimer() 
	{
		start = std::chrono::high_resolution_clock::now();
	}

	void tick() 
	{
		using std::chrono::time_point,
		      std::chrono::high_resolution_clock;
		time_point<high_resolution_clock> now = high_resolution_clock::now();
		last_delta = now - start;
		start = now;
	}

	template <typename T>
	T get_delta() 
	{
		return std::chrono::duration<T>(last_delta).count();
	}
};

void Application::handle_arguments() 
{
	if (appdata.args.size() > 0) {
		string command = "";

		for (string& arg : appdata.args)
			command += arg + " ";

		command.pop_back();

		vector<string> commands;
		int start = 0;
		bool in_command = false;
		int i;
		for (i = 0; i < command.size(); i++) {
			if (!in_command) {
				if (command[i] == '-') {
					start = i + 1;
					in_command = true;
					continue;
				}
				continue;
			}
			if (command[i] == '-') {
				commands.push_back(command.substr(start, i - start));
				start = i + 1;
			}
		}
		commands.push_back(command.substr(start, i - start));

		if (commands.size() == 0)
			gsh::execute_command(command);

		for (auto& cmd : commands) {
			if (cmd.size() > 0)
				gsh::execute_command(cmd);
		}
	}
}

int Application::run() 
{
	try {
		init();
	} catch (const std::runtime_error& e) {
		log_error("Caught runtime error while initializing: \n\t%s\nQuitting...", e.what());
		return 1;
	} 

	bool use_display_timing = renderer->enable_display_timing;
	uint64_t screen_refresh = 0; // invalid if not use_display_timing
	DeltaTimer timer;

	if (use_display_timing)
		screen_refresh = renderer->get_refresh_cycle_duration();
	else
		timer = DeltaTimer();
	uint16_t target_multiple = 1;

	while (running) {
		//std::this_thread::sleep_for(std::chrono::milliseconds(7));
		handle_events();
		gsh::execute_commands();

		if (use_display_timing){
			current_world.run_systems(get_delta(screen_refresh, target_multiple));
			//run_systems(get_delta(screen_refresh, target_multiple));
			renderer->draw_frame(get_present_time(get_time_ns(), screen_refresh, target_multiple), current_world);
			screen_refresh = renderer->get_refresh_cycle_duration();
		} else {
			timer.tick();
			current_world.run_systems(timer.get_delta<float>());
			//run_systems(timer.get_delta<float>());
			renderer->draw_frame(0, current_world);
		}

		//renderer->print_frame_info();
	}

	return 0;
}

void Application::end() 
{
	running = false;
}

Application::Application(ApplicationData d, GameState* s) 
		: appdata(d)
		, current_state(s) 
		, current_world(World(s))
		, renderer(new Renderer(d)) 
	{ app = this; }

void handle_signal(int signum) 
{
	Application::get_app()->end();
}

void Application::init() 
{
#ifdef _WIN32
#ifndef NDEBUG
	freopen("CON", "w", stdout);
	freopen("CON", "w", stderr);
#endif
#endif
	gsh::init();
	for (auto& path : appdata.autoexec_files) {
		try {
			gsh::execute_command(std::string("exec ") + path);
		} catch (const std::runtime_error& e) {}
	}
	handle_arguments();

	log_info("Starting Application '%s'", appdata.name);

	signal(SIGABRT, handle_signal);
	signal(SIGINT,  handle_signal);
	signal(SIGTERM, handle_signal);

	SDL_Init(SDL_INIT_VIDEO);
	IMG_Init(IMG_INIT_PNG);
	renderer->init();
	log_info("Initialized renderer");

	current_state->init(current_world);
	log_info("Initialized state");

	renderer->running = &running;

	log_info("End init");
	initialization_success = true;
}

Application::~Application() 
{
	if (initialization_success) {
		renderer->wait_idle();
		current_world.cleanup();
		current_state->cleanup();
		delete renderer;
		gsh::cleanup();
	}
	SDL_Quit();
}
}
