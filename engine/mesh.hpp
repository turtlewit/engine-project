#pragma once

#include "renderer.hpp"
#include <glm/fwd.hpp>

extern const char _binary_mesh_vert_glsl_spv_start;
extern const char _binary_mesh_vert_glsl_spv_end;

extern const char _binary_mesh_frag_glsl_spv_start;
extern const char _binary_mesh_frag_glsl_spv_end;

namespace engine {
class MeshRenderer;

typedef uint32_t MeshHandle;
typedef uint32_t TextureHandle;

struct UniformBufferObject {
	glm::mat4 model;
	glm::mat4 view;
	glm::mat4 proj;
	glm::vec3 light_position;
};

void MeshRenderSystem(World& w, const VulkanContext& context, uint32_t image_index);

struct Mesh {
	DeviceBuffer vertex_buffer;
	DeviceBuffer index_buffer;
	uint32_t index_count;
	Texture texture;
	uint32_t instances;
	std::vector<std::vector<vk::DescriptorSet>> descriptor_sets;
};

struct MeshComponent {
	MeshHandle handle;
	uint32_t instance;
	uint32_t entity_id;
};

class MeshRenderer : public RenderPassCommandBuffer, public RenderPipeline, public Descriptor {
	std::vector<vk::CommandBuffer> command_buffers;
	vk::DescriptorSetLayout descriptor_set_layout;
	vk::DescriptorPool descriptor_pool;

	// TODO: this should be in resources
	std::vector<Mesh> meshes;

	std::vector<DeviceBuffer> uniform_buffers;
	vk::ShaderModule vert_shader;
	vk::ShaderModule frag_shader;

	Texture default_texture;
	// TODO: this should be in resources
	std::vector<Texture> textures;

	public:
	static MeshRenderer* singleton;

	uint32_t total_instances = 0;
	// RenderPassCommandBuffer impl
	void allocate_command_buffers(const VulkanContext& context) override;
	void record_command_buffer(const VulkanContext& context, 
	                           vk::CommandBufferInheritanceInfo inheritance_info, 
	                           uint32_t image_index) override;
	std::vector<vk::CommandBuffer> get_command_buffers() override;
	void free_command_buffers(const VulkanContext& context) override;

	// RenderPipeline impl
	void create_pipeline(const VulkanContext& context) override;
	void destroy_pipeline(const VulkanContext& context) override;

	// Descriptor impl
	void create_descriptors(const VulkanContext& context) override;

	MeshHandle add_mesh(const std::string& path);
	TextureHandle load_texture(const char* path);
	void add_instance(World& w, Entity e, MeshHandle handle);
	void set_mesh_texture(MeshHandle mesh, TextureHandle texture);
	vk::DeviceMemory get_uniform_buffer_memory(uint32_t index);

	void cleanup();

	MeshRenderer() {}
	MeshRenderer(World& w);
};
}
