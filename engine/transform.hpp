#pragma once

#include <glm/fwd.hpp>

namespace engine {
struct Transform2D {
	float x = 0.0;
	float y = 0.0;
};

struct Transform3D {
	glm::vec3 translation = glm::vec3(0.f);
	glm::vec3 rotation = glm::vec3(0.f);
	glm::vec3 direction = glm::vec3(0.f);
	glm::vec3 scale = glm::vec3(1.f);
	glm::mat4 matrix = glm::mat4(0.f);

	glm::mat4& get_transform();
	glm::vec3& get_translation();
	void set_translation(const glm::vec3& translation_, bool fp = false);
	void update_transform();
	void update_fp();
	void look_at(glm::vec3 pos);
	void rotate(const glm::vec3& axis, const float& angle);
	void rotate_fp(const glm::vec3& axis, const float& angle);
	void set_scale(glm::vec3 scale_);

	Transform3D();
	Transform3D(const glm::vec3& translation_);
};
}
