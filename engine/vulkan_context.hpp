#pragma once

#include <vulkan/vulkan.hpp>

struct QueueFamilies {
	uint32_t graphics = -1;
	uint32_t present = -1;
};

struct VulkanContext {
	vk::Instance instance;
	vk::SurfaceKHR surface;
	vk::PhysicalDevice physical_device;
	vk::Device device;
	vk::DispatchLoaderDynamic dispatch_loader;
	QueueFamilies queue_families;
	vk::Queue graphics_queue;
	vk::CommandPool command_pool;

	vk::Extent2D swapchain_image_extent;
	vk::Format surface_format;
	uint32_t images;
	vk::RenderPass render_pass;
	vk::RenderPass post_process_render_pass;
};

