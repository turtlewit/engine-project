#pragma once

#include <functional>

#include "vulkan_context.hpp"

namespace engine {
class World;

using System = std::function<void(World& world, float)>;
using RenderSystem = std::function<void(World& world, VulkanContext context, uint32_t image_index)>;

}
