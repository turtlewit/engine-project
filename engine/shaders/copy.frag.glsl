#version 450
#extension GL_ARB_separate_shader_objects : enable

layout (set = 0, binding = 0) uniform sampler2D uSourceTex;
layout (location = 0) out vec4 out_color;

layout(push_constant) uniform ScreenSize {
    float w;
    float h;
} screen_size;

void main(void)
{
    out_color = texture(uSourceTex, gl_FragCoord.xy / vec2(screen_size.w, screen_size.h));
		out_color.w = 1.0;
}
