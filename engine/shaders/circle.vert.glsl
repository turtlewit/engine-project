#version 450
#extension GL_ARB_separate_shader_objects : enable

//layout(location = 0) in uvec2 in_position;
layout(location = 0) in vec2 uv;

layout(location = 0) out vec2 o_uv;

layout(binding = 0) uniform ScreenPosition {
	uint radius;
	ivec2 pos;
	uvec2 extent;
} spos;

void main() {
	vec2 pos = vec2(
		(((float(spos.pos.x) + float(uv.x * spos.radius * 2)) / float(spos.extent.x)) - 0.5) * 2.0,
		(((float(spos.pos.y) + float(uv.y * spos.radius * 2)) / float(spos.extent.y)) - 0.5) * 2.0);
	gl_Position = vec4(pos, 0.f, 1.f);
	o_uv = uv;
}
