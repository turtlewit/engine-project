#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 uv;

void main() {
	vec2 pos = vec2(0.0);
	pos.x = (uv.x - 0.5) * 2.0;
	pos.y = (uv.y - 0.5) * 2.0;
	gl_Position = vec4(pos, 0.f, 1.f);
}
