#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 uv;
layout(location = 0) out vec4 out_color;

layout(push_constant) uniform Color {
	vec4 color;
} c;

void main() {
	float a = 0.5 - sqrt(pow(0.5-uv.x, 2) + pow(0.5-uv.y,2));
	a = ((a / abs(a)) + 1.f) / 2.f;
	out_color = vec4(c.color.xyz, a * c.color.w);
}
