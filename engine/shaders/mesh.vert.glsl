#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 2) in vec2 uv;

layout(location = 0) out vec2 uv_out;
layout(location = 1) out float light;

layout(set = 0, binding = 0) uniform UBO {
	mat4 model;
	mat4 view;
	mat4 proj;
	vec3 light_position;
} ubo;


void main() {
	uv_out = uv;
	gl_Position = ubo.proj * ubo.view * ubo.model * vec4(position, 1.);
	vec3 world_normal = normalize(mat3(ubo.model) * normal);
	vec3 world_position = mat3(ubo.model) * position;
	
	vec3 L = normalize(ubo.light_position - world_position);
	
	light = max(0, dot(L, world_normal));
}
