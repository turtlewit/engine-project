#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 1) uniform sampler2D tex; 

layout(location = 0) in vec2 uv;
layout(location = 1) in float light;
layout(location = 0) out vec4 out_color;

void main() {
	vec4 albedo_color = vec4(max(vec3(1.f) * light, vec3(0.2)), 1.f);
	vec4 tex_color = texture(tex, uv);
	
	out_color = albedo_color * tex_color;
}
