#pragma once
#include <typeindex>
#include <typeinfo>
#include <string>
#include <cstdio>

#include "gameshell.hpp"
#include "logger.hpp"

namespace engine::gsh {

extern std::unordered_map<std::string, std::string> var_map;

struct SetVerb : Verb {
	void function(CommandInfo info) override;
};

void set_var(const std::string& key, const std::string& value);
const std::string& get_var_raw(const std::string& key, const std::string& default_value);

template<typename T>
T convert_from_string(std::string& from)
{
	std::type_index ti = typeid(T);
	if (ti == typeid(int))
		return std::stoi(from);
	if (ti == typeid(long int))
		return std::stol(from);
	if (ti == typeid(unsigned int))
		return std::stoul(from);
	if (ti == typeid(long long int))
		return std::stoll(from);
	if (ti == typeid(long long unsigned int))
		return std::stoull(from);
	if (ti == typeid(float))
		return std::stof(from);
	if (ti == typeid(double))
		return std::stod(from);
	if (ti == typeid(long double))
		return std::stold(from);
	if (ti == typeid(bool))
		return from == "true";
	char buffer[256];
	sprintf(buffer, "Could not convert string to %s", typeid(T).name());
	throw std::invalid_argument(buffer);
}

template<typename T>
T get_var(const std::string& key, const T& default_value)
{
	if (var_map.find(key) == var_map.end())
		return default_value;
	return convert_from_string<T>(var_map[key]);
}
}
