#pragma once

#include <SDL2/SDL.h>

#include "world.hpp"

namespace engine {

class GameState;
class Renderer;

struct ApplicationData {
	const char* name = "";
	const uint16_t resolution[2] = {0};
	std::vector<std::string> args;
	std::vector<std::string> autoexec_files;
};

struct KeyboardKey {
	SDL_Keycode keycode;
	bool press;
	bool hold;
	bool release;
};

class Input {
	std::unordered_map<std::string, std::vector<uint32_t>> keymap;
	std::unordered_map<SDL_Keycode, uint32_t> keycode_map;
	std::vector<KeyboardKey> keys;
	std::vector<SDL_Keycode> keycodes;

	public:
	void register_action(const std::string& name, SDL_Keycode keycode);
	bool get_action_down(const std::string& name);
	bool get_action(const std::string& name);
	bool get_action_up(const std::string& name);
	void set_key_down(SDL_Keycode keycode);
	void set_key_up(SDL_Keycode keycode);
	const std::vector<SDL_Keycode> get_keycodes();
	void reset();
	long int mouse_rel_x;
	long int mouse_rel_y;
	bool mouse_click = false;
};

class Application {
	static Application* app;
	bool running = true;
	bool initialization_success = false;
	ApplicationData appdata;
	GameState* current_state;
	World current_world;
	Renderer* renderer;
	void handle_events();
	void handle_arguments();

	public:
	void init();
	static Application* get_app();
	static Renderer* get_renderer();
	static std::array<uint32_t, 2> get_screen_size();
	static Input input;
	int run();
	void end();

	Application();
	Application(ApplicationData d, GameState* s);
	~Application();
};
}
