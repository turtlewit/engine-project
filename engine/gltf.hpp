#pragma once

#include "renderer.hpp"

extern "C" {
struct cgltf_data;
struct cgltf_accessor;
}

namespace engine {
class GltfModel {

	cgltf_data* data;
	cgltf_accessor* index_accessor = nullptr;
	cgltf_accessor* position_accessor = nullptr; 
	cgltf_accessor* normal_accessor = nullptr;
	cgltf_accessor* uv_accessor = nullptr;

	public:
	DeviceBuffer create_vertex_buffer(const VulkanContext& context);
	DeviceBuffer create_index_buffer(const VulkanContext& context);
	uint32_t get_index_count();

	GltfModel(const std::string& path);
	~GltfModel();
};
}
