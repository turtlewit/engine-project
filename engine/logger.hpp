#pragma once 

#include <string>
#include <vector>

namespace engine {
enum LogLevel {NONE, ERROR, INFO};

extern std::string error_prefix;
extern std::string error_color_prefix;
extern std::string error_color_postfix;
extern std::string info_prefix;
extern std::string clear_line;
extern LogLevel log_level;

extern void log_info(const std::string m);
extern void log_error(const std::string m); 
extern void log_info(const char* m, ...);
extern void log_error(const char* m, ...);
extern void log_info(std::vector<const char*> m);
extern void log_error(std::vector<const char*> m);
extern void log_info(const char* m, std::vector<const char*> d);
extern void log_error(const char* m, std::vector<const char*> d);
extern void log_update(const std::string m);
extern void log_update(const char* m, ...);
}
