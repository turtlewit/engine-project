#include <iostream>
#include <sstream>
#include <cstring>
#include <cstdarg>
#include <cstdio>

#include "logger.hpp"

#define MINLOGLEVEL( ll ) \
	if ( log_level < LogLevel::ll ) \
		return;

using std::string, 
      std::cout,
      std::endl,
      std::vector,
      std::flush;

namespace engine {
string error_prefix = "ERR: ";
string error_color_prefix = "\033[38;2;255;0;0m";
string error_color_postfix = "\033[0m";
string info_prefix = "INFO: ";
string clear_line = "\033[2K";
LogLevel log_level = LogLevel::INFO;

void log_info(const string m) 
{
	MINLOGLEVEL (INFO);

	cout << info_prefix << m << endl;
}

void log_error(const string m) 
{
	MINLOGLEVEL (ERROR);

	if (log_level < LogLevel::ERROR)
		return;

	cout << error_color_prefix << error_prefix << m << error_color_postfix << endl;
}

void log_info(const char* m, ...) 
{
	MINLOGLEVEL (INFO);

	va_list args;
	va_start(args, m);

	printf("%s", info_prefix.c_str());
	vprintf(m, args);
	printf("\n");
	fflush(stdout);

	va_end(args);
}

void log_error(const char* m, ...) 
{
	MINLOGLEVEL (ERROR);

	va_list args;
	va_start(args, m);

	printf("%s%s", error_color_prefix.c_str(), error_prefix.c_str());
	vprintf(m, args);
	printf("%s\n", error_color_postfix.c_str());

	va_end(args);
}

void log_info(vector<const char*> m) 
{
	MINLOGLEVEL (INFO);

	log_info(string(m[0]));
	string tab = string(4, ' ');
	for (int i = 1; i < m.size(); i++) {
		log_info(tab + string(m[i]));
	}
}

void log_error(vector<const char*> m) 
{
	MINLOGLEVEL (ERROR);

	log_error(string(m[0]));
	string tab = string(4, ' ');
	for (int i = 1; i < m.size(); i++) {
		log_error(tab + string(m[i]));
	}
}

void log_info(const char* m, vector<const char*> d) 
{
	MINLOGLEVEL (INFO);

	log_info(string(m));
	string tab = string(4, ' ');
	for (auto dl : d) {
		log_info(tab + string(dl));
	}
}

void log_error(const char* m, vector<const char*> d) 
{
	MINLOGLEVEL (ERROR);

	log_error(string(m));
	string tab = string(4, ' ');
	for (auto dl : d) {
		log_error(tab + string(dl));
	}
}

void log_update(const string m) 
{
	MINLOGLEVEL (INFO);

	cout << clear_line << "\r" << info_prefix << m << "\r" << flush;
}

void log_update(const char* m, ...) 
{
	MINLOGLEVEL (INFO);

	va_list args;
	va_start(args, m);

	printf("%s\r%s", clear_line.c_str(), info_prefix.c_str());
	vprintf(m, args);
	printf("\r");
	fflush(stdout);
	
	va_end(args);
}
}
