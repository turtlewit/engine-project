#include "post_process.hpp"

using namespace vk;

using std::vector;

namespace engine {

void PostProcessRenderer::allocate_command_buffers(const VulkanContext& context)
{
	command_buffers = context.device.allocateCommandBuffers(
		{ context.command_pool,
		  CommandBufferLevel::eSecondary,
		  context.images
		}
	);
}

void PostProcessRenderer::record_command_buffer(const VulkanContext& context, vk::CommandBufferInheritanceInfo inheritance_info, uint32_t image_index)
{
	CommandBuffer subbuffer = command_buffers[image_index];
	subbuffer.begin(
		{ CommandBufferUsageFlagBits::eRenderPassContinue 
		  | CommandBufferUsageFlagBits::eSimultaneousUse, 
		  &inheritance_info
		}
	);

	subbuffer.bindPipeline(PipelineBindPoint::eGraphics, pipeline);
	float screen_size[2] = {float(context.swapchain_image_extent.width), float(context.swapchain_image_extent.height)};
	subbuffer.pushConstants(
		layout, 
		ShaderStageFlagBits::eFragment, 
		0, 
		sizeof(float) * 2, 
		screen_size
	);
	subbuffer.bindIndexBuffer(index_buffer.buffer, 0, IndexType::eUint16);
	subbuffer.bindVertexBuffers(
		0, 
		std::array<Buffer, 1>({vertex_buffer.buffer}), 
		std::array<DeviceSize, 1>({(DeviceSize) 0})
	);
	subbuffer.bindDescriptorSets(PipelineBindPoint::eGraphics, layout, 0, descriptor_sets[image_index], {});
	subbuffer.drawIndexed(quad.indices.size(), 1, 0, 0, 0);
	subbuffer.end();
}

std::vector<vk::CommandBuffer> PostProcessRenderer::get_command_buffers()
{
	return command_buffers;
}

void PostProcessRenderer::free_command_buffers(const VulkanContext& context)
{
	context.device.freeCommandBuffers(context.command_pool, command_buffers);
}

void PostProcessRenderer::create_pipeline(const VulkanContext& context)
{
	GraphicsPipelineCreateInfo pipeline_create_info;
	pipeline_create_info.layout = layout;
	pipeline_create_info.stageCount = 2;
	PipelineShaderStageCreateInfo vert_stage (
		{},
		ShaderStageFlagBits::eVertex,
		vert_shader,
		"main",
		nullptr
	);
	PipelineShaderStageCreateInfo frag_stage (
		{},
		ShaderStageFlagBits::eFragment,
		frag_shader,
		"main",
		nullptr
	);
	PipelineShaderStageCreateInfo shader_stages[2] = {vert_stage, frag_stage};
	pipeline_create_info.pStages = shader_stages;

	auto binding_description = vertex::get_vertex_binding_description<vertex::Quad>();
	auto attribute_descriptions = vertex::Quad::get_attribute_descriptions();
	PipelineVertexInputStateCreateInfo vertex_input (
		{},
		1,
		&binding_description,
		attribute_descriptions.size(),
		attribute_descriptions.data()
	);
	pipeline_create_info.pVertexInputState = &vertex_input;

	PipelineRasterizationStateCreateInfo rasterization_state (
		{},
		VK_FALSE, // depth clamp
		VK_FALSE, // rasterizer discard
		PolygonMode::eFill,
		CullModeFlagBits::eBack,
		FrontFace::eClockwise,
		VK_FALSE, // depth bias
		0.f, // depth bias constant factor
		0.f, // depth bias clamp
		0.f, // depth bias slope factor
		1.f // line width
	);
	pipeline_create_info.pRasterizationState = &rasterization_state;

	PipelineDepthStencilStateCreateInfo depth_stencil (
		{},
		false,
		false,
		CompareOp::eLess,
		false,
		false
	);
	pipeline_create_info.pDepthStencilState = &depth_stencil;

	pipeline_create_info.renderPass = context.post_process_render_pass;

	PipelineColorBlendAttachmentState color_blending_attachment (
		VK_TRUE,
		BlendFactor::eSrcAlpha,
		BlendFactor::eOneMinusSrcAlpha,
		BlendOp::eAdd,
		BlendFactor::eOne,
		BlendFactor::eZero,
		BlendOp::eAdd,
		ColorComponentFlagBits::eR
		| ColorComponentFlagBits::eG
		| ColorComponentFlagBits::eB
		| ColorComponentFlagBits::eA
	);

	PipelineColorBlendStateCreateInfo color_blending (
		{},
		VK_FALSE,
		LogicOp::eCopy,
		1,
		&color_blending_attachment,
		{0.f, 0.f, 0.f, 0.f}
	);
	pipeline_create_info.pColorBlendState = &color_blending;

	pipeline = create_graphics_pipeline(context, pipeline_create_info);
}

void PostProcessRenderer::destroy_pipeline(const VulkanContext& context)
{
	context.device.destroyPipeline(pipeline);
}

void PostProcessRenderer::create_descriptors(const VulkanContext& context, vector<Texture> color_attachments)
{
	DescriptorPoolSize sampler_size (DescriptorType::eCombinedImageSampler, context.images);
	descriptor_pool = context.device.createDescriptorPool(
		{ {},
		  context.images,
		  1,
		  &sampler_size
		}
	);

	vector<DescriptorSetLayout> set_layouts (context.images, descriptor_set_layout);
	descriptor_sets = context.device.allocateDescriptorSets(
		{ descriptor_pool,
		  context.images,
		  set_layouts.data()
		}
	);

	vector<DescriptorImageInfo> image_infos;
	image_infos.resize(context.images);
	for (int i = 0; i < context.images; i++) {
		image_infos[i].sampler = color_attachments[i].sampler;
		image_infos[i].imageView = color_attachments[i].image_view;
		image_infos[i].imageLayout = ImageLayout::eShaderReadOnlyOptimal;

		WriteDescriptorSet sampler_write (
			descriptor_sets[i],
			0,
			0,
			1,
			DescriptorType::eCombinedImageSampler,
			&image_infos[i],
			nullptr,
			nullptr
		);
		context.device.updateDescriptorSets(1, &sampler_write, 0, nullptr);
	}
}

void PostProcessRenderer::destroy_descriptors(const VulkanContext& context)
{
	context.device.destroyDescriptorPool(descriptor_pool);
}

void PostProcessRenderer::cleanup()
{
	Renderer* r = Application::get_renderer();
	const VulkanContext& context = r->get_context();

	context.device.destroyPipelineLayout(layout);
	context.device.destroyDescriptorSetLayout(descriptor_set_layout);

	vertex_buffer.free(context.device);
	index_buffer.free(context.device);

	context.device.destroyShaderModule(vert_shader);
	context.device.destroyShaderModule(frag_shader);
}

PostProcessRenderer::~PostProcessRenderer()
{
}

PostProcessRenderer::PostProcessRenderer(const VulkanContext& context, const char* fragment_start, const char* fragment_end)
{
	Renderer* r = Application::get_renderer();

	quad = Quad();
	vertex_buffer = create_vertex_buffer(context, sizeof(vertex::Quad) * quad.vertices.size(), (void*) quad.vertices.data());
	index_buffer = create_index_buffer(context, sizeof(uint16_t) * quad.indices.size(), (void*) quad.indices.data());

	vert_shader = load_shader_binary(context.device, &_binary_post_process_vert_glsl_spv_start, &_binary_post_process_vert_glsl_spv_end);
	frag_shader = load_shader_binary(context.device, fragment_start, fragment_end);

	PushConstantRange push_constant_range (
		ShaderStageFlagBits::eFragment,
		0,
		sizeof(float) * 2
	);

	DescriptorSetLayoutBinding sampler_descriptor_binding (
		0,
		DescriptorType::eCombinedImageSampler,
		1,
		ShaderStageFlagBits::eFragment,
		nullptr
	);

	DescriptorSetLayoutCreateInfo descriptor_set_layout_create_info (
		{},
		1,
		&sampler_descriptor_binding
	);

	descriptor_set_layout = context.device.createDescriptorSetLayout(descriptor_set_layout_create_info);

	PipelineLayoutCreateInfo layout_create_info (
		{},
		1,
		&descriptor_set_layout,
		1,
		&push_constant_range
	);

	layout = context.device.createPipelineLayout(layout_create_info);

	r->add_pipeline(this);
	r->add_post_process_command_buffer(this);
	r->add_post_process_descriptor(this);
}

}
