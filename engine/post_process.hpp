#pragma once

#include "renderer.hpp"

extern const char _binary_post_process_vert_glsl_spv_start;
extern const char _binary_post_process_vert_glsl_spv_end;

namespace engine {

class PostProcessRenderer : public PostProcessCommandBuffer, public RenderPipeline, public PostProcessDescriptor {
	std::vector<vk::CommandBuffer> command_buffers;
	vk::DescriptorSetLayout descriptor_set_layout;
	vk::DescriptorPool descriptor_pool;
	Quad quad;
	DeviceBuffer vertex_buffer;
	DeviceBuffer index_buffer;
	vk::ShaderModule vert_shader;
	vk::ShaderModule frag_shader;

	std::vector<vk::DescriptorSet> descriptor_sets;

	public:
	// RenderPassCommandBuffer impl
	void allocate_command_buffers(const VulkanContext& context) override;
	void record_command_buffer(const VulkanContext& context, 
	                           vk::CommandBufferInheritanceInfo inheritance_info, 
	                           uint32_t image_index) override;
	std::vector<vk::CommandBuffer> get_command_buffers() override;
	void free_command_buffers(const VulkanContext& context) override;

	// RenderPipeline impl
	void create_pipeline(const VulkanContext& context) override;
	void destroy_pipeline(const VulkanContext& context) override;

	// Descriptor impl
	void create_descriptors(const VulkanContext& context, std::vector<Texture> color_attachments) override;
	void destroy_descriptors(const VulkanContext& context) override;

	void cleanup();

	~PostProcessRenderer();
	PostProcessRenderer(const VulkanContext& context, const char* fragment_start, const char* fragment_end);
};
}
