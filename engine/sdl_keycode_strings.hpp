#pragma once

#include <unordered_map>

#include <SDL2/SDL.h>

namespace engine {

const extern std::unordered_map<std::string, SDL_Keycode> keycode_map;

}
