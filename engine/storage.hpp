#pragma once

#include <vector>
#include <unordered_map>
#include <tuple>

#include "unordered_vector.hpp"

namespace engine {

class Storage {
public:
	virtual ~Storage()
	{
	}
};

template <typename T>
class TypeStorage : public Storage {
public:
	~TypeStorage() override
	{
	}

	void add_component(Entity e, const T& c)
	{
		entity_map[e] = storage.get_size();
		storage.add(c);
		entities.add(e);
	}

	void remove_component(T& component)
	{
		size_t index = static_cast<size_t>(&component - storage.begin());
		Entity& entity = get_entity(component);
		entity_map[*(entities.end() - 1)] = index;
		storage.remove(component);
		entities.remove(entity);
	}

	T& get_component(Entity e)
	{
		return *(storage.begin() + entity_map[e]);
	}

	UnorderedVector<T>& get_storage()
	{
		return storage;
	}

	Entity& get_entity(T& component)
	{
		return *(entities.begin() + static_cast<size_t>(&component - storage.begin()));
	}

private:
	UnorderedVector<T> storage;
	UnorderedVector<Entity> entities;
	std::unordered_map<Entity, size_t> entity_map;
};

class StorageManager {
public:

	// Virtual destructor will be called
	// on runtime type and clean up storage.
	~StorageManager()
	{
		for (auto s : storages)
			delete s;
	}

	template<typename T>
	void add_component(const T& c)
	{
		get_component_vector<T>().push_back(c);
	}

	template<typename T>
	UnorderedVector<T>& get_component_vector()
	{
		TypeStorage<T>& storage = get_storage<T>();
		return storage.get_storage();
	}

	template<typename T>
	TypeStorage<T>& get_storage()
	{
		TypeStorage<T>* storage = nullptr;
		for (Storage* s : storages) {
			if (typeid(*s) == typeid(TypeStorage<T>)) {
				storage = static_cast<TypeStorage<T>*>(s);
				break;
			}
		}

		if (storage == nullptr) {
			storage = new TypeStorage<T>();
			storages.push_back(storage);
		}

		return *storage;
	}

	void cleanup()
	{
		for (auto s : storages)
			delete s;
		storages.clear();
	}

private:
	std::vector<Storage*> storages;
};

template <typename... T>
class ComponentTuple {
public:
	ComponentTuple(const std::tuple<T*...> tuple)
		: tuple{tuple}
	{
	}

	template <size_t I>
	friend typename std::tuple_element<I, ComponentTuple>::type& get(ComponentTuple t)
	{
		return *std::get<I>(t.tuple);
	}

private:
	std::tuple<T*...> tuple;
};

}

namespace std {

template <typename... T>
class tuple_size<engine::ComponentTuple<T...>> {
public:
	static constexpr size_t value = sizeof...(T);
};

template <size_t I, typename... T>
struct tuple_element<I, engine::ComponentTuple<T...>> {
	typedef typename tuple_element<I, tuple<T...>>::type type;
};

} // namespace std
