#include <stdexcept>

#include "vars.hpp"
#include "logger.hpp"

using std::string;

namespace engine::gsh {

std::unordered_map<string, string> var_map;

void SetVerb::function(CommandInfo info) {
	if (info.arguments.size() < 2)
		throw std::invalid_argument("\"Set\" verb requires 2 arguments.");
	string& key = info.arguments[0];
	string value = info.arguments[1];
	for (int i = 2; i < info.arguments.size(); i++)
		value += string(" ") + info.arguments[i];
	set_var(key, value);
}

void set_var(const string& key, const string& value)
{
	var_map[key] = value;
}

const string& get_var_raw(const string& key, const string& default_value)
{
	if (var_map.find(key) == var_map.end())
		return default_value;
	return var_map[key];
}
}
