#pragma once

#include <stdexcept>

#include <SDL2/SDL.h>
#include <SDL2/SDL_surface.h>
#include <vulkan/vulkan.hpp>

#include "logger.hpp"

namespace engine {
struct ImageInfo {
	vk::Format format;
	vk::Extent2D extent;
	uint32_t size;
	void* data;

	~ImageInfo() 
	{
		free(data);
	}
};


ImageInfo load_image(const char* path);
}
