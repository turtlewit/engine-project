#include <string>
#include <vector>
#include <stdexcept>

#include "gltf.hpp"
#include "logger.hpp"

#define CGLTF_IMPLEMENTATION
#include <cgltf.h>


using std::string,
      std::vector,
      std::byte;

namespace engine{
DeviceBuffer GltfModel::create_vertex_buffer(const VulkanContext& context) 
{
	vector<float> vertex_buffer_data;
	int position_size = int(position_accessor->buffer_view->size / sizeof(float));

	// uv data is 2/3s the size of position data
	vertex_buffer_data.resize((position_size * 2) + (int(position_size / 3) * 2));
	const float* position_data = 
		(float*) &( reinterpret_cast<byte*>(data->buffers[0].data)[position_accessor->buffer_view->offset] );
	const float* normal_data = 
		(float*) &( reinterpret_cast<byte*>(data->buffers[0].data)[normal_accessor->buffer_view->offset] );

	if (uv_accessor != nullptr) {
		const float* uv_data = 
			(float*) &(reinterpret_cast<byte*>(data->buffers[0].data)[uv_accessor->buffer_view->offset]);

		int j = 0;
		int k = 0;
		for (int i = 0; i < vertex_buffer_data.size(); i += 8) {
			float* vertex_position = &vertex_buffer_data[i];
			float* vertex_normal = &vertex_buffer_data[i+3];
			float* vertex_uv = &vertex_buffer_data[i+6];

			memcpy(vertex_position, &position_data[j], sizeof(float) * 3);
			memcpy(vertex_normal, &normal_data[j], sizeof(float) * 3);
			memcpy(vertex_uv, &uv_data[k], sizeof(float) * 2);

			j += 3;
			k += 2;
		}
	} else {
		int j = 0;
		for (int i = 0; i < vertex_buffer_data.size(); i += 8) {
			float* vertex_position = &vertex_buffer_data[i];
			float* vertex_normal = &vertex_buffer_data[i+3];

			memcpy(vertex_position, &position_data[j], sizeof(float) * 3);
			memcpy(vertex_normal, &normal_data[j], sizeof(float) * 3);

			j += 3;
		}
	}

	return engine::create_vertex_buffer(context, vertex_buffer_data.size() * sizeof(float), vertex_buffer_data.data());
}

DeviceBuffer GltfModel::create_index_buffer(const VulkanContext& context) 
{
	return engine::create_index_buffer(context, 
	                                   index_accessor->buffer_view->size, 
	                                   &reinterpret_cast<byte*>(data->buffers[0].data)[index_accessor->buffer_view->offset]);
}

uint32_t GltfModel::get_index_count() 
{
	return uint32_t(index_accessor->buffer_view->size / sizeof(uint16_t));
}

GltfModel::GltfModel(const string& path) 
{
	log_info("starting to load model");
	cgltf_options options = cgltf_options();
	cgltf_result r = cgltf_parse_file(&options, path.c_str(), &data);
	if (r > 0)
		log_error("cgltf_result error: %d. path: %s", r, path.c_str());
	log_info("parsed file");
	cgltf_load_buffers(&options, data, path.c_str());
	cgltf_primitive primitive = data->meshes[0].primitives[0];
	index_accessor = primitive.indices;

	for (int i = 0; i < primitive.attributes_count; i++) {
		cgltf_attribute attr = primitive.attributes[i];
		switch (attr.type) {
		case cgltf_attribute_type_position:
			position_accessor = attr.data;
			break;
		case cgltf_attribute_type_normal:
			normal_accessor = attr.data;
			break;
		case cgltf_attribute_type_texcoord:
			uv_accessor = attr.data;
			break;
		}
	}

	if (index_accessor == nullptr || position_accessor == nullptr || normal_accessor == nullptr)
		throw std::runtime_error("Gltf model invalid or unsupported");
}

GltfModel::~GltfModel() {
	cgltf_free(data);
}
}
