#include <SDL2/SDL_image.h>

#include "image.hpp"

using namespace vk; 

namespace engine {
vk::Format get_format(uint32_t from) 
{
	vk::Format f;
	switch (from) {
		case SDL_PIXELFORMAT_RGBA8888:
			f = vk::Format::eR8G8B8A8Unorm;
			break;
	}
	if (f == vk::Format())
		throw std::runtime_error("Unrecognized pixel format!");
	return f;
}

ImageInfo load_image(const char* path) 
{
	ImageInfo image_info;
	SDL_Surface* image = IMG_Load(path);

	if (!image) {
		log_error("%s", SDL_GetError());
		throw std::runtime_error("Could not open image from path!");
	}

	int intended_pitch = image->w * 8;
	image_info.data = malloc(image->h * intended_pitch);
	if (image->format->format == SDL_PIXELFORMAT_RGB24) {
		// convert pixel data
		uint8_t* components = reinterpret_cast<uint8_t*>(image->pixels);
		uint32_t* dst_pixels = reinterpret_cast<uint32_t*>(image_info.data);
		int j = 0;
		for (int i = 0; i < image->pitch * image->h; i+=3) {
			dst_pixels[j] = uint32_t( 
				255 << 24 
				| (uint32_t(components[i+2]) << 16  )
				| (uint32_t(components[i+1]) << 8   ) 
				| (uint32_t(components[i])/* << 0 */) 
			);
			j++;
		}
	}
	image_info.format = get_format(SDL_PIXELFORMAT_RGBA8888);
	image_info.extent = Extent2D(image->w, image->h);
	image_info.size = image->h * intended_pitch;
	SDL_FreeSurface(image);

	return image_info;
}
}
