#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/rotate_vector.hpp>

#include "transform.hpp"
#include "logger.hpp"

const glm::vec3 UP = glm::vec3(0.0, -1.0, 0.0);
const glm::vec3 RIGHT = glm::vec3(1.0, 0.0, 0.0);
const glm::vec3 FORWARD = glm::vec3(0.0, 0.0, 1.0);

namespace engine {
glm::mat4& Transform3D::get_transform() 
{
	return matrix;
}

glm::vec3& Transform3D::get_translation() 
{
	return translation;
}

void Transform3D::set_translation(const glm::vec3& translation_, bool fp) 
{
	translation = translation_;
	if (fp)
		update_fp();
	else
		update_transform();
}

void Transform3D::update_transform() 
{
	matrix = glm::translate(glm::mat4(1.0f), translation);
	matrix = glm::rotate(matrix, rotation.x, RIGHT); 
	matrix = glm::rotate(matrix, rotation.y, UP); 
	matrix = glm::rotate(matrix, rotation.z, FORWARD); 
	matrix = glm::scale(matrix, glm::vec3(-scale.x, -scale.y, scale.z));
}

void Transform3D::update_fp() 
{
	matrix = glm::lookAt(translation, translation + direction, UP);
}

void Transform3D::look_at(const glm::vec3 pos) 
{
	matrix = glm::lookAt(translation, pos, UP);
}

void Transform3D::rotate(const glm::vec3& axis, const float& angle) 
{
	rotation += axis * angle;
	rotation = glm::mod(rotation, glm::vec3(glm::pi<float>() * 2));
	update_transform();
}

void Transform3D::rotate_fp(const glm::vec3& axis, const float& angle) 
{
	direction = glm::rotate(direction, angle, axis);
	update_fp();
}

void Transform3D::set_scale(glm::vec3 scale_) 
{
	scale = scale_;
	update_transform();
}

Transform3D::Transform3D() 
{
	set_translation(glm::vec3(0.0));
	set_scale(glm::vec3(1.0));
	direction = FORWARD;
}

Transform3D::Transform3D(const glm::vec3& translation_) 
{
	set_translation(translation_);
	set_scale(glm::vec3(1.0));
}
}
