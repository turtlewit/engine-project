#pragma once

#include <vector>
#include <typeindex>
#include <typeinfo>
#include <unordered_map>
#include <tuple>
#include <functional>

#include "entity.hpp"
#include "component.hpp"
#include "logger.hpp"
#include "bitset.hpp"
#include "storage.hpp"
#include "system.hpp"

namespace engine {

class GameState;

class World {
public:
	World(GameState* s);

	Entity add_entity();

	void add_system(const System& s);

	void add_system(const RenderSystem& s);

	void run_systems(float delta);

	void run_render_systems(VulkanContext context, uint32_t image_index);

	void cleanup();

public:
	template <typename T>
	UnorderedVector<T>& get_components();

	template <typename T>
	T& get_component(Entity e);

	template <typename... T>
	std::vector<ComponentTuple<T...>> get_component_tuples();

	template <typename T>
	void add_component(Entity e, const T& c = T());

	template <typename T>
	void remove_component(T& component);

private:
	std::vector<System> systems;
	std::vector<RenderSystem> render_systems;
	GameState* state;

	StorageManager storage_manager;
	std::vector<Entity> entities;

	std::unordered_map<std::type_index, uint32_t> type_map;
	std::unordered_map<Entity, BitSet> entity_map;
	uint32_t current_type_index;
	
private:
	template <typename T>
	uint32_t get_type_index();

	template <typename... T, typename... Indices>
	std::vector<ComponentTuple<T...>> get_component_tuples_internal(Indices... indices);
};

template <typename T>
void World::add_component(Entity e, const T& c)
{
	uint32_t component_type_index = get_type_index<T>();

	TypeStorage<T>& storage = storage_manager.get_storage<T>();
	storage.add_component(e, c);
	entity_map[e].add(component_type_index);
}

template <typename T>
void World::remove_component(T& component)
{
	TypeStorage<T>& storage = storage_manager.get_storage<T>();
	entity_map[storage.get_entity(component)].remove(type_map[typeid(T)]);
	storage.remove_component(component);
}


template <typename T>
T& World::get_component(Entity e) 
{
	TypeStorage<T>& storage = storage_manager.get_storage<T>();
	return storage.get_component(e);
}

template <typename T>
UnorderedVector<T>& World::get_components() 
{
	return storage_manager.get_component_vector<T>();
}

template <typename... T, typename... Indices>
std::vector<ComponentTuple<T...>> World::get_component_tuples_internal(Indices... indices)
{
	std::vector<ComponentTuple<T...>> components;

	// we can eat the 800 bytes for now.
	// will be improved with an allocator later.
	components.reserve(100); 
	for (Entity e : entities) {
		const BitSet& entity_bits = entity_map.at(e);
		bool not_has_all = ((!entity_bits.contains(indices)) || ...);

		if (not_has_all)
			continue;

		ComponentTuple<T...> tuple {
			std::make_tuple(&storage_manager.get_storage<T>().get_component(e)...)
				//&storage_manager.get_storage<T>()
				//.storage[storage_manager.get_storage<T>().entity_map.at(e)]...)
		};
		components.push_back(tuple);
	}
	return std::move(components);
}

template <typename... T>
std::vector<ComponentTuple<T...>> World::get_component_tuples()
{
	return std::move(get_component_tuples_internal<T...>(type_map.at(typeid(T))...));
}

template <typename T>
uint32_t World::get_type_index()
{
	std::type_index i = typeid(T);
	auto pair = type_map.find(i);
	if (pair != type_map.end())
		return pair->second;

	type_map[i] = current_type_index;
	return current_type_index++;
}

} // namespace engine
