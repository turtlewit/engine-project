#include <thread>

#include "world.hpp"

namespace engine {

World::World(GameState* s) 
	: state(s), current_type_index{0}
{
}

Entity World::add_entity() 
{
	entities.push_back(Entity {(Entity) entities.size()});
	entity_map[entities.back()] = BitSet{};
	return entities.back();
}

void World::add_system(const System& s) 
{
	systems.push_back(s);
}

void World::add_system(const RenderSystem& s) 
{
	render_systems.push_back(s);
}

namespace {
	struct ThreadData {
		System system;
		World* world;
		float delta;
	};

	void thread_job(ThreadData data)
	{
		data.system(*data.world, data.delta);
	}
}

void World::run_systems(float delta)
{
	unsigned int nthreads = std::thread::hardware_concurrency();
	if (nthreads == 0)
		nthreads = 4;
	//else
	//	nthreads -= 1;

	unsigned int current_system = 0;
	std::vector<std::thread> threads{nthreads};
	while (current_system < systems.size()) {
		for (unsigned int i = 0; i < nthreads; ++i) {
			if (current_system < systems.size()) {
				threads[i] = std::thread{thread_job, 
				                         ThreadData{systems[current_system++],
				                                    this,
				                                    delta}};
			} else {
				break;
			}
		}
		for (auto& thread : threads) {
			if (thread.joinable())
				thread.join();
			if (current_system < systems.size()) {
				thread = std::thread{thread_job, 
				                     ThreadData{systems[current_system++],
				                                this,
				                                delta}};
			}
		}
	}
	for (auto& thread : threads) {
		if (thread.joinable())
			thread.join();
	}

	//for (auto& system : systems)
	//	system(*this, delta);
}

void World::run_render_systems(VulkanContext context, uint32_t image_index)
{
	for (auto& system : render_systems)
		system(*this, context, image_index);
}

void World::cleanup() 
{
	storage_manager.cleanup();
	entities.clear();
	type_map.clear();
	entity_map.clear();
	current_type_index = 0;
}

} // namespace engine
