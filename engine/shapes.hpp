#pragma once

#include <glm/fwd.hpp>

#include "system.hpp"
#include "renderer.hpp"

extern const char _binary_circle_vert_glsl_spv_start;
extern const char _binary_circle_vert_glsl_spv_end;
extern const char _binary_circle_frag_glsl_spv_start;
extern const char _binary_circle_frag_glsl_spv_end;

namespace engine{

class Transform2D;
class World;
class CircleRenderer;

struct ScreenPosition {
	uint32_t radius;
	alignas(8) glm::ivec2 pos;
	glm::uvec2 extent;
};

struct CircleComponent {
	uint32_t radius;
	uint32_t entity_id;
};

void CircleSystem(World& w, const VulkanContext& context, uint32_t image_index);

struct Color {
	float r, g, b, a;
	Color(float r_, float g_, float b_, float a_ = 1.0) : 
		r(r_), g(g_), b(b_), a(a_) {}
};

class CircleRenderer : public RenderPassCommandBuffer, public RenderPipeline, public Descriptor {
	std::vector<vk::CommandBuffer> command_buffers;
	vk::DescriptorSetLayout descriptor_set_layout;
	vk::DescriptorPool descriptor_pool;
	Quad quad;
	DeviceBuffer vertex_buffer;
	DeviceBuffer index_buffer;
	vk::ShaderModule vert_shader;
	vk::ShaderModule frag_shader;
	std::vector<DeviceBuffer> uniform_buffers;

	std::vector<vk::DescriptorSet> descriptor_sets;
	std::vector<Color> colors;

	public:
	static CircleRenderer* singleton;
	// RenderPassCommandBuffer impl
	void allocate_command_buffers(const VulkanContext& context) override;
	void record_command_buffer(const VulkanContext& context, 
	                           vk::CommandBufferInheritanceInfo inheritance_info, 
	                           uint32_t image_index) override;
	std::vector<vk::CommandBuffer> get_command_buffers() override;
	void free_command_buffers(const VulkanContext& context) override;

	// RenderPipeline impl
	void create_pipeline(const VulkanContext& context) override;
	void destroy_pipeline(const VulkanContext& context) override;

	// Descriptor impl
	void create_descriptors(const VulkanContext& context) override;

	void add_circle(World& w, Entity e, unsigned int radius, Color color);
	vk::DeviceMemory get_uniform_buffer_memory(uint32_t index);

	void cleanup();

	CircleRenderer() {}
	CircleRenderer(World& w);
};
}
