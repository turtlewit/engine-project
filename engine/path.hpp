#pragma once
#include <string>

namespace engine {
inline const std::string make_path(const std::string s) {
#ifdef _WIN32
	std::string n (s);
	for (char& c : n) {
		if (c == '/')
			c = '\\';
	}
	return n;
#else
	return s;
#endif
}
}
