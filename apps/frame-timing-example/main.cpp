#include <vector>
#include <string>

#include <glm/glm.hpp>

#include <engine.hpp>
#include <transform.hpp>
#include <world.hpp>
#include <entity.hpp>
#include <system.hpp>
#include <component.hpp>
#include <application.hpp>
#include <shapes.hpp>
#include <logger.hpp>
#include <renderer.hpp>

using namespace engine;

struct SyncCircleComponent {
	float height;
	uint32_t radius;
	float speed;
	SyncCircleComponent(float height, uint32_t radius, float speed) : height(height), radius(radius), speed(speed) {}
};

struct GroupA {};
struct GroupB {};
struct GroupC {};
struct GroupD {};
struct GroupE {};
struct GroupF {};
struct GroupG {};
struct GroupH {};

void update_circle(float delta, SyncCircleComponent& circle, Transform2D& transform, unsigned int width, unsigned int height)
{
	float speed = circle.speed * width * delta;
	transform.y = ((float) height * circle.height) - circle.radius;
	float new_x = transform.x + speed;
	if (new_x < (float) ((float) -2 * circle.radius) - 1) 
		new_x = (float) width - 1;
	if (new_x > width + 1)
		new_x = (float) -2 * circle.radius;
	transform.x = new_x;
}

#define CIRCLEUPDATE( name, group ) \
void name(World& w, float delta) \
{ \
	auto resolution = Application::get_screen_size(); \
	unsigned int width = resolution[0]; \
	unsigned int height = resolution[1]; \
\
	for (auto& [circle, transform, gr] : w.get_component_tuples<SyncCircleComponent, Transform2D, group>()) { \
		update_circle(delta, circle, transform, width, height); \
	} \
}

CIRCLEUPDATE( CircleUpdateA, GroupA )
CIRCLEUPDATE( CircleUpdateB, GroupB )
CIRCLEUPDATE( CircleUpdateC, GroupC )
CIRCLEUPDATE( CircleUpdateD, GroupD )
CIRCLEUPDATE( CircleUpdateE, GroupE )
CIRCLEUPDATE( CircleUpdateF, GroupF )
CIRCLEUPDATE( CircleUpdateG, GroupG )
CIRCLEUPDATE( CircleUpdateH, GroupH )


class MainState : public GameState {
	CircleRenderer* cr;
	public:
	void init(World& world) override 
	{
		cr = new CircleRenderer(world);
		//log_info("pointer %x", &cr);

		Entity e = world.add_entity();
		cr->add_circle(world, e, 100, Color(1.f, 0.f, 0.f)); 
		auto sync = SyncCircleComponent(0.5, 100, 1.0);
		world.add_component<SyncCircleComponent>(e, sync);
		world.add_component(e, GroupA{});

#define NUMBER 1000000

		for (int i = 0; i < NUMBER; i++) {
			int radius = rand() % 10;
			float r = (float) rand() / RAND_MAX;
			float g = (float) rand() / RAND_MAX;
			float b = (float) rand() / RAND_MAX;
			float speed = (((float) rand() / RAND_MAX) - 0.5) * 1.0;
			//float speed = 1;
			float height = (float) rand() / RAND_MAX;

			e = world.add_entity();
			cr->add_circle(world, e, radius, Color(r, g, b)); 
			//world.add_component(e, Transform2D{});
			sync = SyncCircleComponent(height, radius, speed);
			world.add_component<SyncCircleComponent>(e, sync);
			if (i < (NUMBER / 8))
				world.add_component(e, GroupA{});
			else if (i < (NUMBER / 8) * 2)
				world.add_component(e, GroupB{});
			else if (i < (NUMBER / 8) * 3)
				world.add_component(e, GroupC{});
			else if (i < (NUMBER / 8) * 4)
				world.add_component(e, GroupD{});
			else if (i < (NUMBER / 8) * 5)
				world.add_component(e, GroupE{});
			else if (i < (NUMBER / 8) * 6)
				world.add_component(e, GroupF{});
			else if (i < (NUMBER / 8) * 7)
				world.add_component(e, GroupG{});
			else
				world.add_component(e, GroupH{});

		}

		world.add_system(CircleUpdateA);
		world.add_system(CircleUpdateB);
		world.add_system(CircleUpdateC);
		world.add_system(CircleUpdateD);
		world.add_system(CircleUpdateE);
		world.add_system(CircleUpdateF);
		world.add_system(CircleUpdateG);
		world.add_system(CircleUpdateH);
	}
	void cleanup() override
	{
		cr->cleanup();
	}
};

int main (int argc, char** argv) 
{
	std::vector<std::string> args;
	args.assign(argv + 1, argv + argc);
	ApplicationData appdata = {
		"Frame Timing Example",
		{800, 600},
		args
	};

	MainState main_state = MainState();

	Application app = Application(appdata, &main_state);
	return app.run();
}

