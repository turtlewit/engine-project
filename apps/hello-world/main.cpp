#include <iostream>
#include <vector>

#include <engine.hpp>
#include <world.hpp>
#include <entity.hpp>
#include <system.hpp>
#include <component.hpp>
#include <application.hpp>
#include <logger.hpp>

using namespace engine;

struct HelloComponent {
	const char* message;
};

struct OtherComponent {
	int my_number;
};

void HelloSystem(World& w, float delta)
{
	for (auto [h, o] : w.get_component_tuples<HelloComponent, OtherComponent>()) {
	}
}

void add_main_entity(World& w, const char* message) 
{
	Entity e = w.add_entity();
	HelloComponent c = {message};
	OtherComponent oc = {0};
	w.add_component<HelloComponent>(e, c);
	w.add_component<OtherComponent>(e, oc);
}

void add_only_hello(World& w, const char* message) 
{
	w.add_component<HelloComponent>(w.add_entity(), {message});
}

void add_other_entity(World& w) 
{
	w.add_component<OtherComponent>(w.add_entity(), {1});
}

class HelloState : public GameState {
	public:
	void init(World& world) override 
	{
		Entity e = world.add_entity();
		HelloComponent c = {"I shouldn't appear"};
		world.add_component<HelloComponent>(e, c);

		for (int i = 0; i < 60000; i++) {
			add_only_hello(world, "Can't see me!");
		}
		for (int i = 0; i < 60000; i++) {
			add_main_entity(world, "Yoo!!");
		}
		for (int i = 0; i < 60000; i++) {
			add_other_entity(world);
		}


		world.add_system(HelloSystem);
		world.add_system(HelloSystem);
		world.add_system(HelloSystem);
		world.add_system(HelloSystem);
		world.add_system(HelloSystem);
		world.add_system(HelloSystem);
		world.add_system(HelloSystem);
		world.add_system(HelloSystem);
	}

	void cleanup() override {};
};

int main (int argc, char** argv) 
{
	ApplicationData appdata = {
		"Hello World",
		{800, 600}
	};

	HelloState main_state = HelloState();

	Application app = Application(appdata, &main_state);
	return app.run();
}

