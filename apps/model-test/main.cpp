#include <vector>
#include <string>

#include <glm/glm.hpp>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mouse.h>

#include <application.hpp>
#include <camera.hpp>
#include <entity.hpp>
#include <light.hpp>
#include <logger.hpp>
#include <path.hpp>
#include <transform.hpp>
#include <vars.hpp>

#include "main.hpp"

using namespace engine;

void RotateSystem(World& w, float delta)
{
	static bool has_been_deleted = false;
	auto tuples = w.get_component_tuples<MeshUpdateComponent, Transform3D>();
	for (auto& [update, transform] : tuples) {
		update.time += delta;
		update.time = glm::mod(update.time, 3.1415f * 2.f);
		transform.set_translation(
			glm::vec3(sin(update.time) + update.initial_offset, 0.f, 0.f));
		transform.rotate(glm::vec3(0.0, -1.0, 0.0), update.speed * delta);
		if (Application::input.get_action_down("del") && !has_been_deleted)
		{
			has_been_deleted = true;
			w.remove_component(update);
		}
	}
}

void FlyCamera(World& w, float delta)
{
	auto tuples = w.get_component_tuples<CameraComponent, Transform3D>();
	for (auto& [camera, transform] : tuples) {
		transform.rotate_fp(
			glm::vec3(0.f, -1.f, 0.f), glm::radians(-float(Application::input.mouse_rel_x) / 10.0));
		glm::vec3 direction = transform.direction;
		glm::vec3 right = glm::normalize(glm::vec3(direction.z, 0., -direction.x));
		glm::vec3 up = glm::vec3(0., -1., 0.);
		transform.rotate_fp(right, glm::radians(-float(Application::input.mouse_rel_y) / 10.0));

		float speed = 0;
		if (Application::input.get_action("fast"))
			speed = 10.0 * delta;
		else
			speed = 5.0 * delta;

		if (Application::input.get_action("forward"))
			transform.set_translation(transform.translation + (direction * speed), true);
		if (Application::input.get_action("backward"))
			transform.set_translation(transform.translation - (direction * speed), true);
		if (Application::input.get_action("left"))
			transform.set_translation(transform.translation - (right * speed), true);
		if (Application::input.get_action("right"))
			transform.set_translation(transform.translation + (right * speed), true);
		if (Application::input.get_action("up"))
			transform.set_translation(transform.translation + (up * speed), true);
		if (Application::input.get_action("down"))
			transform.set_translation(transform.translation - (up * speed), true);
	}
}

void MouseCapture(World& w, float delta) {
	for (auto& c : w.get_components<MouseCaptureComponent>()) {
		if (c.captured) {
			if (Application::input.get_action("esc")) {
				SDL_SetRelativeMouseMode(SDL_FALSE);
				c.captured = false;
			}
		} else {
			if (Application::input.mouse_click) {
				SDL_SetRelativeMouseMode(SDL_TRUE);
				c.captured = true;
			}
		}
	}
}

void MainState::init(World& world) 
{
	SDL_SetRelativeMouseMode(SDL_TRUE);

	mesh_renderer = new MeshRenderer(world);
	MeshHandle m = mesh_renderer->add_mesh(make_path("assets/circle.gltf").c_str());
	MeshHandle cube = mesh_renderer->add_mesh(make_path("assets/uv-mapped-cube.gltf").c_str());
	TextureHandle catra = mesh_renderer->load_texture(make_path("assets/catra.png").c_str());
	mesh_renderer->set_mesh_texture(cube, catra);

	Entity ball_entity = world.add_entity();
	mesh_renderer->add_instance(world, ball_entity, m);
	auto& ball_transform = world.get_component<Transform3D>(ball_entity);
	ball_transform.set_translation(glm::vec3(-2.0, 0.0, 0.0));
	world.add_component<MeshUpdateComponent>(ball_entity, {-2.0, 1.0});

	Entity cube_entity = world.add_entity();
	mesh_renderer->add_instance(world, cube_entity, cube);
	auto& cube_transform = world.get_component<Transform3D>(cube_entity);
	cube_transform.set_translation(glm::vec3(2.0, 0.0, 0.0));
	world.add_component<MeshUpdateComponent>(cube_entity, {2.0, 0.5});

	Entity light_entity = world.add_entity();
	LightComponent light_component = LightComponent();
	Transform3D light_transform = Transform3D();
	light_transform.set_translation(glm::vec3(2.f, -2.f, 0.f));
	world.add_component<LightComponent>(light_entity, light_component);
	world.add_component<Transform3D>(light_entity, light_transform);

	Entity camera_entity = world.add_entity();
	auto [width, height] = Application::get_screen_size();
	CameraComponent camera_component (glm::radians(90.0f), float(width), float(height));
	Transform3D camera_transform = Transform3D();
	camera_transform.set_translation(glm::vec3(0.f, -0.f, -5.f));
	camera_transform.look_at(glm::vec3(0.f, 0.f, 0.f));
	world.add_component<CameraComponent>(camera_entity, camera_component);
	world.add_component<Transform3D>(camera_entity, camera_transform);

	Entity mce = world.add_entity();
	world.add_component<MouseCaptureComponent>(mce, MouseCaptureComponent());

	world.add_system(RotateSystem);
	world.add_system(FlyCamera);
	world.add_system(MouseCapture);
}

void MainState::cleanup() 
{
	mesh_renderer->cleanup();
	SDL_SetRelativeMouseMode(SDL_FALSE);
}

MainState::~MainState()
{
	delete mesh_renderer;
}

int main(int argc, char** argv) 
{
	std::vector<std::string> args;
	args.assign(argv + 1, argv + argc);
	ApplicationData appdata = {
		"Model Test",
		{800, 600},
		args,
		{std::string("autoexec.gsh")}
	};

	MainState main_state = MainState();

	Application app = Application(appdata, &main_state);
	return app.run();
}
