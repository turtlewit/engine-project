#pragma once

#include <engine.hpp>
#include <mesh.hpp>
#include <system.hpp>
#include <world.hpp>

struct MeshUpdateComponent {
	float initial_offset;
	float speed;
	float time;
};

struct MouseCaptureComponent {
	bool captured = true;
};

class MainState : public engine::GameState {
	engine::MeshRenderer* mesh_renderer;

	public:
	void init(engine::World& world) override;
	void cleanup() override;
	~MainState() override;
};

