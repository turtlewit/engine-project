cmake_minimum_required (VERSION 3.0)
project (hello-world)

add_custom_target (
	autoexec
	COMMAND cp -u ${CMAKE_CURRENT_SOURCE_DIR}/autoexec.gsh ${CMAKE_BINARY_DIR}/
)

add_executable (modeltest main.cpp)
add_dependencies(modeltest autoexec)

include_directories ("../../engine/")
target_link_libraries (modeltest engine ${SDL2_LIBRARIES} ${Vulkan_LIBRARIES})
