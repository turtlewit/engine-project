# autoexec for modeltest

setkey forward  SDLK_w SDLK_UP
setkey backward SDLK_s SDLK_DOWN
setkey left     SDLK_a SDLK_LEFT
setkey right    SDLK_d SDLK_RIGHT
setkey up       SDLK_SPACE
setkey down     SDLK_LCTRL
setkey fast     SDLK_LSHIFT
setkey esc      SDLK_ESCAPE
setkey del      SDLK_DELETE

# renderer settings
set use_display_timing false # requires VK_GOOGLE_display_timing
set use_anisotropy true # anisotropic filtering
set max_anisotropy 16 # max anisotropic filtering
set use_mipmapping true # mipmapping
set use_fxaa true # fxaa
set device_index 0 # Change to select a different graphics device
